mysqldump: [Warning] Using a password on the command line interface can be insecure.
-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: giantgroove
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `band`
--

DROP TABLE IF EXISTS `band`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `band` (
  `id` int NOT NULL AUTO_INCREMENT,
  `manager_id` int DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_48DFA2EB783E3463` (`manager_id`),
  KEY `IDX_48DFA2EBB03A8386` (`created_by_id`),
  CONSTRAINT `FK_48DFA2EB783E3463` FOREIGN KEY (`manager_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_48DFA2EBB03A8386` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `band`
--

LOCK TABLES `band` WRITE;
/*!40000 ALTER TABLE `band` DISABLE KEYS */;
INSERT INTO `band` VALUES (1,1,1,'La bande à Picsou','la-bande-a-picsou','Ouh ouh ouh',NULL,NULL,'2023-03-19 21:37:15',NULL),(3,2,2,'Rusted Metal Unicorn','rusted-metal-unicorn','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis finibus ipsum eu blandit. Nam rutrum varius mauris eget tempus. Praesent a dignissim massa, ac laoreet lorem. In hac habitasse platea dictumst. Pellentesque rutrum libero ante, nec imperdiet justo finibus non. Etiam sit amet leo elementum, finibus metus sed, hendrerit mi. Maecenas ante est, semper eu nisl sed, luctus vulputate ex. Curabitur suscipit lorem vel sapien venenatis aliquam. Nullam tempus justo id arcu mollis volutpat. Quisque venenatis arcu urna, egestas tincidunt massa fermentum eu. Aliquam erat mauris, hendrerit non est a, mollis tincidunt odio. Proin in sagittis erat. Nulla nulla neque, consectetur a metus in, blandit dignissim metus. Cras mollis at tellus quis vulputate. Pellentesque nisi mauris, viverra eu vulputate vel, maximus ut ante.',NULL,NULL,'2023-03-19 22:36:06',NULL);
/*!40000 ALTER TABLE `band` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `band_user`
--

DROP TABLE IF EXISTS `band_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `band_user` (
  `band_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`band_id`,`user_id`),
  KEY `IDX_D6A5361249ABEB17` (`band_id`),
  KEY `IDX_D6A53612A76ED395` (`user_id`),
  CONSTRAINT `FK_D6A5361249ABEB17` FOREIGN KEY (`band_id`) REFERENCES `band` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D6A53612A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `band_user`
--

LOCK TABLES `band_user` WRITE;
/*!40000 ALTER TABLE `band_user` DISABLE KEYS */;
INSERT INTO `band_user` VALUES (1,1),(1,2),(3,1),(3,2),(3,3);
/*!40000 ALTER TABLE `band_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20230319220846','2023-03-19 22:08:58',65970);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrument`
--

DROP TABLE IF EXISTS `instrument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instrument` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrument`
--

LOCK TABLES `instrument` WRITE;
/*!40000 ALTER TABLE `instrument` DISABLE KEYS */;
INSERT INTO `instrument` VALUES (1,'Guitare','guitare','2023-03-19 22:41:30','2023-03-19 22:41:30'),(2,'Piano','piano','2023-03-19 22:41:36','2023-03-19 22:41:36'),(3,'Batterie','batterie','2023-03-19 22:41:42','2023-03-19 22:41:42'),(4,'Basse','basse','2023-03-19 22:41:47','2023-03-19 22:41:47'),(5,'Vocal','vocal','2023-03-19 22:41:53','2023-03-19 22:41:53'),(6,'Flûte','flute','2023-03-19 22:42:01','2023-03-22 10:04:10');
/*!40000 ALTER TABLE `instrument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `counter` int NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'New Feature: Seeker','new-feature-seeker','<p>Grande nouvelle:</p>\r\n<p>Il est d&eacute;sormais possible de cliquer sur la piste dans la partie mixage afin de faire un bond dans le temps!</p>','patchnote',1,'2023-03-28 15:25:50','2023-04-02 12:48:09'),(2,'try tinymce','try-tinymce','<p>TinyMCE est maintenant disponible pour les champs de type Textarea.</p>','patchnote',2,'2023-03-29 09:56:27','2023-04-02 13:01:19');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `recipient_id` int DEFAULT NULL,
  `sender_id` int DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BF5476CAE92F8F78` (`recipient_id`),
  KEY `IDX_BF5476CAF624B39D` (`sender_id`),
  CONSTRAINT `FK_BF5476CAE92F8F78` FOREIGN KEY (`recipient_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_BF5476CAF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (1,1,2,'<a href=\"http://giantgroove.loc:8083/user/BlackSavate\">BlackSavate</a> has requested to join La bande à Picsou. Please visit <a href=\"http://giantgroove.loc:8083/band/La-bande-a-Picsou/members/edit\">handle members page</a> to accept or decline.','2023-03-22 09:27:32','read');
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author_id` int DEFAULT NULL,
  `band_id` int DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2FB3D0EEF675F31B` (`author_id`),
  KEY `IDX_2FB3D0EE49ABEB17` (`band_id`),
  CONSTRAINT `FK_2FB3D0EE49ABEB17` FOREIGN KEY (`band_id`) REFERENCES `band` (`id`),
  CONSTRAINT `FK_2FB3D0EEF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (2,1,1,'The trooper','the-trooper','2023-03-20 14:43:23','2023-03-22 10:04:42'),(3,1,NULL,'dzadza','dzadza','2023-03-28 12:41:23',NULL);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `style`
--

DROP TABLE IF EXISTS `style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `style` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `style`
--

LOCK TABLES `style` WRITE;
/*!40000 ALTER TABLE `style` DISABLE KEYS */;
INSERT INTO `style` VALUES (1,'Metal','metal','2023-04-08 13:38:53','2023-04-08 13:38:53'),(2,'Reggae','reggae','2023-04-08 13:39:04','2023-04-08 13:39:04'),(3,'Zouk','zouk','2023-04-08 13:39:11','2023-04-08 13:39:11');
/*!40000 ALTER TABLE `style` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `track_base`
--

DROP TABLE IF EXISTS `track_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `track_base` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int DEFAULT NULL,
  `author_id` int DEFAULT NULL,
  `track_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `discr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_open` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5DE2D664166D1F9C` (`project_id`),
  KEY `IDX_5DE2D664F675F31B` (`author_id`),
  KEY `IDX_5DE2D6645ED23C43` (`track_id`),
  CONSTRAINT `FK_5DE2D664166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `FK_5DE2D6645ED23C43` FOREIGN KEY (`track_id`) REFERENCES `track_base` (`id`),
  CONSTRAINT `FK_5DE2D664F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `track_base`
--

LOCK TABLES `track_base` WRITE;
/*!40000 ALTER TABLE `track_base` DISABLE KEYS */;
INSERT INTO `track_base` VALUES (8,2,1,NULL,'Batterie','batterie',NULL,0,'2023-03-20 14:43:41',NULL,'track_db',1),(9,2,1,NULL,'Guitare','guitare',NULL,0,'2023-03-20 14:43:47',NULL,'track_db',1),(10,2,1,NULL,'Vocal','vocal',NULL,0,'2023-03-20 14:43:53',NULL,'track_db',1),(11,2,1,NULL,'Basse','basse',NULL,0,'2023-03-20 14:43:59',NULL,'track_db',1),(12,NULL,1,8,'drum','drum','8d1fdcaa09985c7f40114c02f439f283.mp3',0,'2023-03-20 14:44:13',NULL,'contribution_db',NULL),(13,NULL,1,9,'all guitars','all-guitars','4a3f17c601f9490976cb3615fe7919f8.mp3',0,'2023-03-20 14:44:25',NULL,'contribution_db',NULL),(15,NULL,1,10,'vocal','vocal','427700258b840b989c7b995965cd0902.mp3',36.1,'2023-03-20 14:45:18',NULL,'contribution_db',NULL),(16,NULL,1,11,'bass','bass','434b9ad2e826452e1bc20f99bf159a6b.mp3',0,'2023-03-20 14:45:26',NULL,'contribution_db',NULL),(17,NULL,2,9,'guitars2','guitars2','7c9e39f64fbafeacad6f58edc09d6b86.mp3',0,'2023-03-22 09:28:48',NULL,'contribution_db',NULL),(28,3,1,NULL,'aze','aze','31168690f9a44deb09afe8dab9365db9.mp3',0,'2023-03-28 14:25:25',NULL,'track_db',0),(29,3,1,NULL,'eaz','eaz','5603fd1d63ad16a5ea7a8b1c496b42ad.mp3',0,'2023-03-28 14:29:11',NULL,'track_db',0),(30,3,1,NULL,'Basse','basse',NULL,0,'2023-03-28 14:30:09',NULL,'track_db',1),(31,NULL,1,30,'bass','bass','c56b120eca892d4059ac4f2c5be96bfb.mp3',0,'2023-03-28 14:30:30',NULL,'contribution_db',NULL),(32,NULL,2,30,'drum','drum','6f7198610cfc898e20401892f18ad102.mp3',0,'2023-03-28 15:10:14',NULL,'contribution_db',NULL);
/*!40000 ALTER TABLE `track_base` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `track_instrument`
--

DROP TABLE IF EXISTS `track_instrument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `track_instrument` (
  `track_id` int NOT NULL,
  `instrument_id` int NOT NULL,
  PRIMARY KEY (`track_id`,`instrument_id`),
  KEY `IDX_866EE7765ED23C43` (`track_id`),
  KEY `IDX_866EE776CF11D9C` (`instrument_id`),
  CONSTRAINT `FK_866EE7765ED23C43` FOREIGN KEY (`track_id`) REFERENCES `track_base` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_866EE776CF11D9C` FOREIGN KEY (`instrument_id`) REFERENCES `instrument` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `track_instrument`
--

LOCK TABLES `track_instrument` WRITE;
/*!40000 ALTER TABLE `track_instrument` DISABLE KEYS */;
INSERT INTO `track_instrument` VALUES (30,4);
/*!40000 ALTER TABLE `track_instrument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` int NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` datetime NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `roles` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'sbelaud','sbelaud','savate43@hotmail.fr','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Simon','BELAUD','1 rue des admins',NULL,86000,'POITIERS','France','1987-03-02 00:00:00',NULL,'2023-04-10 09:37:09','2023-04-10 09:37:09',1,'[\"ROLE_USER\", \"ROLE_ADMIN\"]'),(2,'BlackSavate','blacksavate','blacksavate@outlook.fr','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Simon','BELAUD','2 rue du dev',NULL,86000,'POITIERS','France','1987-03-02 00:00:00',NULL,'2023-03-19 22:23:25','2023-03-19 22:23:25',1,'[\"ROLE_USER\"]'),(3,'Toto 1','toto-1','toto+1@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','un','1 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(4,'Toto 2','toto-2','toto+2@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','deux','2 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(5,'Toto 3','toto-3','toto+3@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','trois','3 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(6,'Toto 4','toto-4','toto+4@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','quatre','4 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(7,'Toto 5','toto-5','toto+5@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','cinq','5 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(8,'Toto 6','toto-6','toto+6@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','six','6 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(9,'Toto 7','toto-7','toto+7@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','sept','7 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(10,'Toto 8','toto-8','toto+8@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','huit','8 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',0,'[\"ROLE_USER\"]'),(11,'Toto 9','toto-9','toto+9@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','neuf','9 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(12,'Toto 10','toto-10','toto+10@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','dix','10 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(13,'Toto 11','toto-11','toto+11@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','onze','11 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(14,'Toto 12','toto-12','toto+12@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','douze','12 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(15,'Toto 13','toto-13','toto+13@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','treize','13 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(16,'Toto 14','toto-14','toto+14@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','quatorze','14 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(17,'Toto 15','toto-15','toto+15@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','quinze','15 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(18,'Toto 16','toto-16','toto+16@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','seize','16 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(19,'Toto 17','toto-17','toto+17@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','dix-sept','17 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(20,'Toto 18','toto-18','toto+18@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','dix-huit','18 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(21,'Toto 19','toto-19','toto+19@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','dix-neuf','19 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(22,'Toto 20','toto-20','toto+20@gmail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Toto','vingt','20 rue du toto',NULL,86000,'Poitiers','France','1987-03-02 00:00:00',NULL,'2023-03-22 09:58:15','2023-03-22 09:58:15',1,'[\"ROLE_USER\"]'),(23,'Vinny','vinny','vinnydubowsky@mail.com','$2y$13$3I/NLmzHxZNxx1Ufy4LTve1Y1CRm35sj.Jl6AmhpbIPYqrtixAana','Vinny','Dubowsky','1','rue de Tamriel',98765,'Blancherive','Bordeciel','1968-02-07 00:00:00',NULL,'2023-06-11 08:10:34','2023-06-11 08:10:34',1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_instrument`
--

DROP TABLE IF EXISTS `user_instrument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_instrument` (
  `user_id` int NOT NULL,
  `instrument_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`instrument_id`),
  KEY `IDX_9BD8AF31A76ED395` (`user_id`),
  KEY `IDX_9BD8AF31CF11D9C` (`instrument_id`),
  CONSTRAINT `FK_9BD8AF31A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_9BD8AF31CF11D9C` FOREIGN KEY (`instrument_id`) REFERENCES `instrument` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_instrument`
--

LOCK TABLES `user_instrument` WRITE;
/*!40000 ALTER TABLE `user_instrument` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_instrument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_style`
--

DROP TABLE IF EXISTS `user_style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_style` (
  `user_id` int NOT NULL,
  `style_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`style_id`),
  KEY `IDX_D17F4332A76ED395` (`user_id`),
  KEY `IDX_D17F4332BACD6074` (`style_id`),
  CONSTRAINT `FK_D17F4332A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D17F4332BACD6074` FOREIGN KEY (`style_id`) REFERENCES `style` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_style`
--

LOCK TABLES `user_style` WRITE;
/*!40000 ALTER TABLE `user_style` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_style` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-11  8:30:02
