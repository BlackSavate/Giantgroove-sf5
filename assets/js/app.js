/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

 const $ = require('jquery');
 require('bootstrap');

import tinymce from 'tinymce';
import 'tinymce/themes/silver';

tinymce.init({
  selector: 'textarea.tinymce',
  skin_url: '/build/skins/custom/ui/giantgroove',
  content_css: '/build/skins/custom/content/giantgroove/content.css',
  icon_url: '/build/icons/default/icons.js',
  models: {
    dom: '/build/models/dom/model.js'
  }
});


 $(document).ready(function() {
     $('[data-toggle="popover"]').popover();
 });
