const $ = require('jquery');
require('jquery-easing');
import '../css/project.scss';

var projectApp = {
    init: function() {
        $('#add-track').on('click', projectApp.addTrack);
        $('#switch-to-mixing').on('click', projectApp.switchToMixing);
        $('#switch-to-studio').on('click', projectApp.switchToStudio);
    },
    addTrack: function(evt) {
        var form = $("#tracks");
        var index = $('#tracks tr:last').data('trackid');
        if(undefined === index) {
            index = 0;
        }
        index++;
        var newTrack = $('<tr>');
        newTrack.attr('data-trackid', index);
        var trackname = $('<td>');
        var tracknameInput = $('<input>');
        tracknameInput.addClass('track');
        tracknameInput.attr('name', 'app_project[tracks][track'+index+']');
        var instru = $('<td>');
        var instruInput = $('<input>');
        instruInput.addClass('instru');
        instruInput.attr('name', 'app_project[tracks][instru'+index+']');
        tracknameInput.attr('placeholder', 'Nom de la piste ' + index);
        instruInput.attr('placeholder', 'Instruments ' + index);
        instru.append(instruInput);
        trackname.append(tracknameInput);
        newTrack.append(trackname);
        newTrack.append(instru);
        form.append(newTrack)
    },
    switchToMixing: function () {
        var transition = $('.container').width();

        $('#studio-container').animate({ right: transition }, 500);
        $('#mixing-container').animate({ right: transition }, 500)
    },
    switchToStudio: function () {
        var transition = $('.container').width();
        $('#mixing-container').animate({ right: 0 }, 500);
        $('#studio-container').animate({ right: 0 }, 500)
    }
};

$(projectApp.init);
