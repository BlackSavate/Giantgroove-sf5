import 'bootstrap/js/dist/modal';
import '../css/admin.scss';



var adminApp = {
  init: () => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const pageUrlParam = urlParams.get('page');
    const orderByUrlParam = urlParams.get('orderBy');
    const orientationUrlParam = urlParams.get('orientation');
    if(orderByUrlParam && orientationUrlParam) {
      $('.sortable[data-order="'+orderByUrlParam+'"]').addClass('arrow-'+orientationUrlParam);
    }
    $('[data-tab="'+activeTab+'"]').addClass('active')
    $('th.sortable').click(adminApp.updateOrder)
  },
  updateOrder: (evt) => {

    const target = $(evt.target);
    const orderBy = target.data('order');
    var orientation = 'asc';

    if(target.hasClass('arrow-asc')) {
      orientation='desc';
    }

    var redirectUrl = location.protocol + '//' + location.host + location.pathname+'?';
    if(adminApp.pageUrlParam) {
      redirectUrl += 'page='+adminApp.pageUrlParam+'&';
    }
    redirectUrl += 'orderBy='+orderBy+'&orientation='+orientation;

    window.location.href = redirectUrl;
  },
};

$(adminApp.init);


