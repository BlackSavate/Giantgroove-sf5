import 'bootstrap/js/dist/modal';
import '../css/band.scss';

var bandApp = {
  init: () => {
    $('#leave-band-btn').click(bandApp.displayLeaveModal);
    $('#confirm-leave-band-link').click(bandApp.confirmLeave);
    $('#update-manager-btn').click(bandApp.displayUpdateManagerModal);
    $('#confirm-update-manager-modal').on('shown.bs.modal', () => {
      $('#confirm-update-manager-btn').focus();
    });
    $('#confirm-update-manager-btn').click(bandApp.confirmUpdateManager);
  },
  displayLeaveModal: () => {
    $('#confirm-leave-band-modal').modal('show');
  },
  confirmLeave: (evt) => {
    evt.preventDefault();
    $('#confirm-leave-band-modal').modal('hide');
    const leaveUrl = $('#confirm-leave-band-link').attr('href');
    $.get(leaveUrl, () => {
      location.reload();
    });
  },
  displayUpdateManagerModal: (evt) => {
    evt.preventDefault();
    $('#confirm-update-manager-modal').modal('show');
  },
  confirmUpdateManager: (evt) => {
    evt.preventDefault();
    $('#confirm-update-manager-modal').modal('hide');
    $('#band-form').submit();
  }
};

$(bandApp.init);


