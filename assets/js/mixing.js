/**
 *   schéma de câblage
 *
 *   | source |----->| panner |----->| gain |----->| destination |
 *
 **/
const $ = require('jquery');
import '../css/mixing.scss';

var mixingApp = {
    audioContext: new (window.AudioContext || window.webkitAudioContext)(),
    audioContextStart:undefined,
    sources: [],
    panners: [],
    gains: [],
    startTimes: [],
    analysers: [],
    originalSVG: {
        'play': $('#play').children('path').attr('d'),
        'pause': $('#pause').children('path').attr('d'),
    },
    init: function() {
        $('#play').on('click', mixingApp.play);
        $('#pause').on('click', mixingApp.pause);
        $('#stop').on('click', mixingApp.stop);
        $('#volumePlus').on('click', mixingApp.volumePlus);
        $('.volumeBar').on('change', mixingApp.changeVolume);
        $('.panBar').on('change', mixingApp.changePan);
        $('.startTimeInput').on('change', mixingApp.updateStartTime);
        $('.colorInput').on('change', mixingApp.changeColor);
        $('.contribution-select').on('change', mixingApp.changeAudio);
        mixingApp.initStartTime();
    },
    initStartTime: function() {
        var startTimes = $('.startTimeInput');
        startTimes.each(function() {
            mixingApp.startTimes['st'+$(this).data('id')] = parseFloat($(this).val());
        });
    },
    getTrackListData: function(data) {
        var trackList = $('tr[data-audio]');
        var keyOffset = 0;
        trackList.each(function() {
            if($(trackList[keyOffset]).attr('data-audio') === '') {
                trackList.splice(keyOffset, 1)
            }
            else {
                keyOffset++;
            }
        });

        // if(!data) {
            return trackList;
        // }
        // var dataVal = [];
        // trackList.each(function() {
        //     dataVal.push($(this).attr('data-'+data))
        // });
        // return dataVal;
    },
    loadTrack: function (track) {
        var audio = window.location.origin + '/' + track.attr('data-audio');
        var id = track.attr('data-id');
        window.fetch(audio)
            .then(response => response.arrayBuffer())
            .then(arrayBuffer => mixingApp.audioContext.decodeAudioData(arrayBuffer))
            .then(audioBuffer => {
                var source = mixingApp.audioContext.createBufferSource();
                var gainNode = mixingApp.audioContext.createGain();
                var stereoPanner = mixingApp.audioContext.createStereoPanner();
                source.buffer = audioBuffer;
                source.startTime = mixingApp.startTimes['st'+id];
                source.onended = mixingApp.onEnd;
                source.connect(gainNode);
                gainNode.connect(stereoPanner);
                stereoPanner.connect(mixingApp.audioContext.destination);
                source.id = id;
                stereoPanner.id = id;
                gainNode.id = id;
                mixingApp.sources['s'+id] = source;
                mixingApp.panners['p'+id] = stereoPanner;
                mixingApp.gains['g'+id] = gainNode;
                mixingApp.changeVolume($('.volumeBar[data-id='+id+']').first());
                mixingApp.changePan($('.panBar[data-id='+id+']').first());
            });
        mixingApp.relativeCurrentTime = 0;
    },
    play: function() {
        var trackList = mixingApp.getTrackListData();
        if(trackList.length === 0) {
            $('#alert-container').html(null);
            var alertMessage = $('<div>')
            alertMessage.text('No track to load. Please select at least one contribution');
            alertMessage.addClass('alert alert-danger update-alert-message');
            $('#alert-container').append(alertMessage)
        }
        else {
            $('.startTimeInput').attr('disabled','disabled');
            $('.startTimeInput').addClass('disabled');
            $('.visual-track-row').on('click', mixingApp.updateCursor);
            $('#play').hide();
            $('#pause').show().focus();
            mixingApp.sources = [];
            mixingApp.gains = [];
            mixingApp.panners = [];
            trackList.each(function () {
                mixingApp.loadTrack($(this))
            });
            var waitLoading = setInterval(function(){
                if(Object.keys(mixingApp.sources).length === trackList.length) {
                    // Tracks are loaded here
                    $('#stop').removeClass('disabled');
                    mixingApp.audioContextStart = mixingApp.audioContext.currentTime;
                    mixingApp.updateCurrentTime();
                    var totalTime = [];
                    clearInterval(waitLoading);
                    for (var track in mixingApp.sources) {
                        var startTime = mixingApp.sources[track].startTime;
                        totalTime.push(mixingApp.sources[track].buffer.duration + startTime);
                        mixingApp.sources[track].start(startTime + mixingApp.audioContextStart);
                    }

                    mixingApp.totalTime = Math.max(...totalTime);

                    for (var track in mixingApp.sources) {
                        var id = track.replace('s', '');
                        var visualTrack = $('#visual-track-' + id);
                        visualTrack.css({
                            'width': ((mixingApp.sources[track].buffer.duration / mixingApp.totalTime) * 100) + '%',
                            'left': ((mixingApp.sources[track].startTime / mixingApp.totalTime * 100) + '%')
                        })
                    }
                    $('#totalTime').html(mixingApp.secondsToDhms(mixingApp.totalTime));
                }
            }, 1500);
        }
    },
    pause: function() {
        if(mixingApp.audioContext.state === 'running') {
            mixingApp.audioContext.suspend();
            $('#pause').children('path').attr('d', mixingApp.originalSVG.play);
        }
        else if (mixingApp.audioContext.state === 'suspended') {
            mixingApp.audioContext.resume();
            $('#pause').children('path').attr('d', mixingApp.originalSVG.pause);
        }
        else {
            console.log('ERROR')
        }
    },
    stop: function() {
        $('.visual-track-row').off('click', mixingApp.updateCursor);
        for (var track in mixingApp.sources) {
            if(mixingApp.audioContext.state === 'suspended') {
                mixingApp.audioContext.resume()
            }
            mixingApp.sources[track].stop();
        };
        mixingApp.relativeCurrentTime = 0;
        mixingApp.currentTime = 0;
        mixingApp.stopTimer();
        $('#pause').children('path').attr('d', mixingApp.originalSVG.play);
        $('#currentTime').html('00:00:00');
        $('.startTimeInput').attr('disabled',false);
        $('.startTimeInput').removeClass('disabled');;
        $('#stop').addClass('disabled');
        $('#play').show().focus();
        $('#pause').hide();
        $('#pause').children('path').attr('d', mixingApp.originalSVG.pause);
        $('#alert-container').html(null)
    },
    onEnd: function(evt) {
        if((evt.target.buffer.duration + evt.target.startTime) == mixingApp.totalTime && !evt.target.preventstop) {
            mixingApp.stop();
        }
    },
    updateCurrentTime: function () {
        mixingApp.currentTimeInterval = setInterval(function(){
            mixingApp.currentTime =  mixingApp.audioContext.currentTime - mixingApp.audioContextStart + mixingApp.relativeCurrentTime;
            var percent = mixingApp.currentTime / mixingApp.totalTime
            $('.visual-track-cursor').css({
                'left': percent*100+'%'
            })
            var seconds = Math.round(mixingApp.currentTime)

            var currentTimeFormated = mixingApp.secondsToDhms(seconds)
            $('#currentTime').html(currentTimeFormated)

            if(mixingApp.currentTime > mixingApp.totalTime) {
                mixingApp.stop()
            }
        }, 100);
    },
    updateCursor: function (evt) {
        var row = $(this);
        var rowOffset = row.offset().left;
        var clickX = event.pageX;
        var relativeX = clickX - rowOffset;
        var width = row.width();
        var relativeXPercent = (relativeX * 100) / width;

        $('.visual-track-cursor').css({
            'left': relativeXPercent*100+'%'
        })

        for (var track in mixingApp.sources) {
            if(mixingApp.audioContext.state === 'suspended') {
                mixingApp.audioContext.resume()
            }

            mixingApp.sources[track].preventstop = true;
            var trackStartTime = mixingApp.sources[track].startTime;
            var trackId = mixingApp.sources[track].id;
            mixingApp.sources[track].stop();
            var audioBuffer = mixingApp.sources[track].buffer;
            var audioBufferSourceNode = mixingApp.audioContext.createBufferSource();
            audioBufferSourceNode.connect( mixingApp.audioContext.destination);
            audioBufferSourceNode.buffer = audioBuffer;
            mixingApp.sources[track] = audioBufferSourceNode;
            mixingApp.sources[track].startTime = trackStartTime;
            mixingApp.sources[track].id = trackId;
            var startTime = 0;
            var offset = mixingApp.totalTime * relativeXPercent / 100;
            var trackStarted = offset > trackStartTime;
            if (trackStarted) {
                offset = offset - trackStartTime;
            }
            else {
                startTime = trackStartTime - offset;
                offset = 0;
            }
            mixingApp.sources[track].start(startTime + mixingApp.audioContext.currentTime, offset);
            mixingApp.sources[track].preventstop = false;
        }
        mixingApp.relativeCurrentTime = mixingApp.totalTime * relativeXPercent / 100 - mixingApp.audioContext.currentTime + mixingApp.audioContextStart;
        mixingApp.disableOnEnd = false;
    },
    secondsToDhms: function (seconds) {
        seconds = Number(seconds);
        var h = Math.floor(seconds % (3600*24) / 3600);
        var m = Math.floor(seconds % 3600 / 60);
        var s = Math.floor(seconds % 60);

        var hDisplay = h < 10 ? '0'+ h : h;
        var mDisplay = m < 10 ? '0'+ m : m;
        var sDisplay = s < 10 ? '0'+ s : s;
        return hDisplay + ':' + mDisplay + ':' + sDisplay;
    },
    stopTimer: function() {
        clearInterval(mixingApp.currentTimeInterval);
    },
    changeVolume: function(evt) {
        var volume =     ($(evt.target).val() != undefined ) ? $(evt.target).val() : evt.val();
        var trackId = ($(evt.target).data('id') != undefined ) ? $(evt.target).data('id') : evt.data('id');
        if(mixingApp.gains['g' + trackId] !== undefined) {
            mixingApp.gains['g' + trackId].gain.value = volume / 50;
        }
    },
    changePan: function(evt) {
        var pan =     ($(evt.target).val() != undefined ) ? $(evt.target).val() : evt.val();
        var trackId = ($(evt.target).data('id') != undefined ) ? $(evt.target).data('id') : evt.data('id');

        if(mixingApp.panners['p'+trackId] !== undefined) {
            mixingApp.panners['p'+trackId].pan.value = pan/100;
        }
    },
    volumePlus: function(evt) {

    },
    updateStartTime: function (evt) {
       var id = $(evt.target).data('id');
       if(parseFloat($(evt.target).val()) < 0)
           $(evt.target).val(0);
       mixingApp.startTimes['st'+id] = parseFloat($(evt.target).val());
    },
    changeColor: function(evt) {
        var id = $(evt.target).data('id');
        var visualTrack = $('#visual-track-'+id);
        visualTrack.css({
            'backgroundColor': $(evt.target).val()
        })
    },
    changeAudio: function(evt) {
        $('#alert-container').html(null);
        if(!$('#stop.disabled')[0]) {
            var alertMessage = $('<div>')
            alertMessage.text('There are changes in the studio. Please stop the playing to load the new setup.');
            alertMessage.addClass('alert alert-danger update-alert-message');
            $('#alert-container').append(alertMessage)
        }
        var audio = $(evt.target).val();
        var trackAudio = $(evt.target).parent().parent();
        var id = trackAudio.attr('data-id');
        var start = $(evt.target).find(":selected").attr("data-start");
        var author = $(evt.target).find(":selected").attr("data-author");
        var authorlink = $(evt.target).parent().siblings('.author').children('a');
        if(author) {
            authorlink.attr('href','/user/'+author);
            authorlink.removeClass('unclickable');
            authorlink.text(author);
        }
        else {
            authorlink.attr('href','#');
            authorlink.addClass('unclickable');
            authorlink.text('-');
        }
        trackAudio.attr('data-audio', audio);
        trackAudio.attr('data-start', start ?? 0);
        trackAudio.attr('data-author', author ?? null);
        if(audio === ''){
            $('.track-parameters-row[data-id='+id+']').hide()
            $('.visual-track-row[data-id='+id+']').hide()
        }
        else{
            mixingApp.startTimes['st'+id] = parseFloat(start);
            $('.track-parameters-row[data-id='+id+'] .startTimeInput').val(start)
            $('.track-parameters-row[data-id='+id+']').show()
            $('.visual-track-row[data-id='+id+']').show()
        }
    },
    // loadAnalyser: function() {
    //     $(mixingApp.sources).each(function(id) {
    //         var analyser = mixingApp.audioContext.createAnalyser();
    //         analyser.id=mixingApp.sources[id].id;
    //         mixingApp.sources[id].connect(analyser)
    //         analyser.fftSize = 2048;
    //         analyser.tailleMemoireTampon = analyser.frequencyBinCount;
    //         analyser.tableauDonnees = new Uint8Array(analyser.tailleMemoireTampon);
    //         analyser.getByteTimeDomainData(analyser.tableauDonnees);
    //         mixingApp.analysers.push(analyser)
    //     })
    //     mixingApp.dessiner();
    // },
    // dessiner: function() {
    //     $(mixingApp.sources).each(function(id) {
    //         // dessine un oscilloscope de la source audio
    //         var trackId = mixingApp.sources[id].id;
    //         // var canvas = $("#oscilloscope-"+trackId).first();
    //         var canvas = document.getElementById("oscilloscope-"+trackId);
    //         var contexteCanvas = canvas.getContext("2d");
    //         var analyser = null;
    //         $(mixingApp.analysers).each(function(key){
    //             if(mixingApp.analysers[key].id == trackId) {
    //                 analyser = mixingApp.analysers[key];
    //             }
    //         })
    //         requestAnimationFrame(mixingApp.dessiner);
    //
    //         analyser.getByteTimeDomainData(analyser.tableauDonnees);
    //
    //         contexteCanvas.fillStyle = 'rgb(200, 200, 200)';
    //         contexteCanvas.fillRect(0, 0, WIDTH, HEIGHT);
    //
    //         contexteCanvas.lineWidth = 2;
    //         contexteCanvas.strokeStyle = 'rgb(0, 0, 0)';
    //
    //         contexteCanvas.beginPath();
    //
    //         var sliceWidth = WIDTH * 1.0 / analyser.tailleMemoireTampon;
    //         var x = 0;
    //
    //         for (var i = 0; i < analyser.tailleMemoireTampon; i++) {
    //
    //             var v = analyser.tableauDonnees[i] / 128.0;
    //             var y = v * HEIGHT / 2;
    //
    //             if (i === 0) {
    //                 contexteCanvas.moveTo(x, y);
    //             } else {
    //                 contexteCanvas.lineTo(x, y);
    //             }
    //
    //             x += sliceWidth;
    //         }
    //
    //         contexteCanvas.lineTo(canvas.width, canvas.height / 2);
    //         contexteCanvas.stroke();
    //     })
    // },
};

$(mixingApp.init);
