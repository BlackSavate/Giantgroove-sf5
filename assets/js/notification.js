import '../css/notifications.scss';

var notificationApp = {
  init: () => {
    $('.mark-read').click(notificationApp.markAsRead)
    $('.mark-unread').click(notificationApp.markAsUnRead)
    $('.notification-delete').click(notificationApp.delete)
  },
  markAsRead: function (evt) {
    var button = $(evt.target);
    var notificationId = button.data('id');
    var actionUrl = "/notifications/read/"+notificationId;
    $.ajax({
      type: 'POST',
      url: actionUrl,
      data: { id: notificationId },
      success: function() {
        button.text('Mark as Unread');
        button.addClass('mark-unread').removeClass('mark-read');
        button.off('click');
        button.click(notificationApp.markAsUnRead);
        var unreadNotificationCount = $('#notification-counter')[0].textContent;
        var newCount = parseInt(unreadNotificationCount)-1;
        $('#notification-counter')[0].textContent = newCount;
        if(newCount === 0) {
          $('#notification-counter-btn').removeClass('btn-danger').addClass('btn-secondary')
        }
      },
      error: function() {
        console.log('error')
        // Handle the error
      }
    });
  },
  markAsUnRead: function (evt) {
    var button = $(evt.target);
    var notificationId = button.data('id');
    var actionUrl = "/notifications/unread/"+notificationId;
    $.ajax({
      type: 'POST',
      url: actionUrl,
      data: { id: notificationId },
      success: function() {
        button.text('Mark as Read');
        button.addClass('mark-read').removeClass('mark-unread');
        button.off('click');
        button.click(notificationApp.markAsRead);
        var unreadNotificationCount = $('#notification-counter')[0].textContent;
        var newCount = parseInt(unreadNotificationCount)+1;
        $('#notification-counter')[0].textContent = newCount;
        if(newCount === 1 ) {
          $('#notification-counter-btn').removeClass('btn-secondary').addClass('btn-danger')
        }
      },
      error: function() {
        console.log('error')
        // Handle the error
      }
    });
  },
  delete: function (evt) {
    var button = $(evt.target);

    var notificationId = button.data('id');
    var actionUrl = "/notifications/"+notificationId;
    $.ajax({
      type: 'DELETE',
      url: actionUrl,
      data: { id: notificationId },
      success: function() {
        button.parent('.notification-container').hide()
      },
      error: function() {
        console.log('error')
        // Handle the error
      }
    });
  }
};

$(notificationApp.init);

