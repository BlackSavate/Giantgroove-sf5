<?php

namespace App\Twig;

use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NotificationExtension extends AbstractExtension
{
    private $notificationRepository;
    private $security;

    public function __construct(UserRepository $userRepository, Security $security)
    {
        $this->userRepository = $userRepository;
        $this->security = $security;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getUnreadNotificationCount', [$this, 'getUnreadNotificationCount']),
        ];
    }

    public function getUnreadNotificationCount()
    {
        $user = $this->security->getUser();

        if (!$user) {
            return 0;
        }

        return $this->userRepository->findUnreadNotifications($user);
    }
}
