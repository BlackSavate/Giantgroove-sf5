<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTimeInterface;

/**
 * Band
 *
 * @ORM\Table(name="admin_history")
 * @ORM\Entity(repositoryClass="App\Repository\AdminHistoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AdminHistory {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private ?int $user_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $action;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $target_type;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private ?int $target_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $target_label;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $time;

    public function __construct() {
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getUserId(): ?int {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self {
        $this->user_id = $user_id;

        return $this;
    }

    public function getUsername(): ?string {
        return $this->username;
    }

    public function setUsername(string $username): self {
        $this->username = $username;

        return $this;
    }

    public function getAction(): ?string {
        return $this->action;
    }

    public function setAction(string $action): self {
        $this->action = $action;

        return $this;
    }

    public function getTargetType(): ?string {
        return $this->target_type;
    }

    public function setTargetType(string $target_type): self {
        $this->target_type = $target_type;

        return $this;
    }

    public function getTargetId(): ?int {
        return $this->target_id;
    }

    public function setTargetId(int $target_id): self {
        $this->target_id = $target_id;

        return $this;
    }

    public function getTargetLabel(): ?string {
        return $this->target_label;
    }

    public function setTargetLabel(string $target_label): self {
        $this->target_label = $target_label;

        return $this;
    }

    public function getTime(): ?DateTimeInterface {
        return $this->time;
    }

    public function setTime(DateTimeInterface $time): self {
        $this->time = $time;

        return $this;
    }
}
