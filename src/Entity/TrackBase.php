<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="track_base")
 * @ORM\Entity(repositoryClass="App\Repository\TrackBaseRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"track_db" = "Track", "contribution_db" = "Contribution"})
 */
abstract class TrackBase
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="audio", type="string", length=255, nullable=true)
     * @Assert\File(
     *    mimeTypes={ "audio/*" },
     *    mimeTypesMessage="Veuillez choisir un fichier dans un format audio valide.",
     *    maxSize="30M")
     */
    private $audio;

    /**
     * @ORM\Column(type="float")
     */
    private $startTime;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="tracks")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tracks")
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $slugger = new AsciiSlugger();
        $this->slug = $slugger->slug(strtolower($slug));

        return $this;
    }

    /**
     * Set audio
     *
     * @param string $audio
     *
     * @return TrackBase
     */
    public function setAudio($audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return string
     */
    public function getAudio()
    {
        return $this->audio;
    }

    /**
     * Set startTime
     *
     * @param float $startTime
     *
     * @return TrackBase
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return float
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Get the value of Project
     *
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set the value of Project
     *
     * @param mixed project
     *
     * @return TrackBase
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get the value of Author
     *
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set the value of Author
     *
     * @param mixed author
     *
     * @return TrackBase
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
