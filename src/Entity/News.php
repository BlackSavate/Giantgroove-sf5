<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * Band
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private string $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private string $message;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private string $type;

    /**
     * @var string
     *
     * @ORM\Column(name="counter", type="integer")
     */
    private string $counter;



    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $updated_at;

    public function __construct() {
        $this->members = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    public function setSlug(string $slug): self {
        $slugger = new AsciiSlugger();
        $this->slug = $slugger->slug(strtolower($slug));

        return $this;
    }

    public function getMessage(): ?string {
        return $this->message;
    }

    public function setMessage(string $message): self {
        $this->message = $message;

        return $this;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }


    public function getCounter(): ?string {
        return $this->counter;
    }

    public function setCounter(string $counter): self {
        $this->counter = $counter;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface {
        return $this->updated_at;
    }

    public function setUpdatedAt(?DateTimeInterface $updated_at): self {
        $this->updated_at = $updated_at;

        return $this;
    }
}
