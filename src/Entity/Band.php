<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * Band
 *
 * @ORM\Table(name="band")
 * @ORM\Entity(repositoryClass="App\Repository\BandRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Band {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private string $description;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private ?string $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="banner",  type="string", length=255, nullable=true)
     */
    private ?string $banner = null;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="bands")
     */
    private mixed $members;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="managedBands")
     */
    private mixed $manager;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="band")
     */
    private $projects;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdBands")
     */
    private mixed $created_by;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $updated_at;

    public function __construct(UserInterface $created_by) {
        $this->members = new ArrayCollection();
        $this->created_by = $created_by;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    public function setSlug(string $slug): self {
        $slugger = new AsciiSlugger();
        $this->slug = $slugger->slug(strtolower($slug));

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getLogo(): ?string {
        return $this->logo ?? null;
    }

    public function setLogo(?string $logo): self {
        // Only update the logo property if a non-null value is provided
        if ($logo !== null) {
            $this->logo = $logo;
        }
        return $this;
    }

    public function removeLogo(): self {
        $this->logo = null;

        return $this;
    }

    public function getBanner(): ?string {
        return $this->banner ?? null;
    }

    public function setBanner(?string $banner): self {
        if ($banner !== null) {
            $this->banner = $banner;
        }
        return $this;
    }
    public function removeBanner(): self {
        $this->banner = null;

        return $this;
    }

    /**
     * Get the value of Members
     *
     * @return mixed
     */
    public function getMembers(): mixed {
        return $this->members;
    }

    /**
     * Set the value of Members
     *
     * @param mixed $members members
     *
     * @return Band
     */
    public function setMembers(mixed $members): static {
        $this->members = $members;

        return $this;
    }

    /**
     * Set the value of Members
     *
     * @param $member
     *
     * @return Band
     */
    public function addMember($member): Band {
        $this->members[] = $member;
        $member->setBand($this);

        return $this;
    }

    /**
     * Set the value of Members
     *
     * @param $member
     *
     * @return Band
     */
    public function removeMember($member): Band {
        $this->members->removeElement($member);

        return $this;
    }

    /**
     * Get the value of Manager
     *
     * @return mixed
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set the value of Manager
     *
     * @param mixed manager
     *
     * @return Band
     */
    public function setManager($manager)
    {
        $this->manager = $manager;

        return $this;
    }


    /**
     * Get the value of projects
     *
     * @return mixed
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set the value of projects
     *
     * @param mixed projects
     *
     * @return User
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;

        return $this;
    }

    /**
     * Get the value of createdBy
     *
     * @return UserInterface
     */
    public function getCreatedBy(): UserInterface
    {
        return $this->created_by;
    }

    public function getCreatedAt(): ?DateTimeInterface {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface {
        return $this->updated_at;
    }

    public function setUpdatedAt(?DateTimeInterface $updated_at): self {
        $this->updated_at = $updated_at;

        return $this;
    }
}
