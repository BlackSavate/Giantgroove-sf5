<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contribution
 *
 * @ORM\Table(name="contribution")
 * @ORM\Entity(repositoryClass="App\Repository\ContributionRepository")
 */
class Contribution extends TrackBase {
    /**
     * @ORM\ManyToOne(targetEntity="Track", inversedBy="contributions", cascade={"persist"})
     */
    private Track $track;

    /**
     * Get the value of Track
     *
     * @return Track
     */
    public function getTrack(): Track {
        return $this->track;
    }

    /**
     * Set the value of Track
     *
     * @param Track $track track
     *
     * @return self
     */
    public function setTrack(Track $track): self {
        $this->track = $track;

        return $this;
    }
}
