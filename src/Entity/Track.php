<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrackRepository")
 */
class Track extends TrackBase
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $isOpen;

    /**
     * @ORM\ManyToMany(targetEntity="Instrument", inversedBy="tracks")
     */
    private $instruments;

    /**
     * @ORM\OneToMany(targetEntity="Contribution", mappedBy="track")
     */
    private $contributions;

    public function getIsOpen(): ?bool
    {
        return $this->isOpen;
    }

    public function setIsOpen(bool $isOpen): self
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    /**
     * Get the value of Instruments
     *
     * @return mixed
     */
    public function getInstruments()
    {
        return $this->instruments;
    }

    /**
     * Set the value of Instruments
     *
     * @param mixed instruments
     *
     * @return Track
     */
    public function setInstruments($instruments)
    {
        $this->instruments = $instruments;

        return $this;
    }

    /**
     * Get the value of Contributions
     *
     * @return mixed
     */
    public function getContributions()
    {
        return $this->contributions;
    }

    /**
     * Set the value of Contributions
     *
     * @param mixed contributions
     *
     * @return self
     */
    public function setContributions($contributions)
    {
        $this->contributions = $contributions;

        return $this;
    }

    /**
     * Set the value of Contributions
     *
     * @param Contribution contribution
     *
     * @return self
     */
    public function removeContribution(Contribution $contribution)
    {
        $this->contributions->removeElement($contribution);
        return $this;
    }

}
