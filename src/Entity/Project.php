<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projects")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="TrackBase", mappedBy="project")
     */
    private $tracks;

    /**
     * @ORM\ManyToOne(targetEntity="Band", inversedBy="projects")
     */
    private $band;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;


    public function __construct()
    {
        $this->tracks = new ArrayCollection;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $slugger = new AsciiSlugger();
        $this->slug = $slugger->slug(strtolower($slug));

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get the value of Author
     *
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set the value of Author
     *
     * @param mixed author
     *
     * @return Project
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the value of Tracks
     *
     * @return mixed
     */
    public function getTracks()
    {
        return $this->tracks;
    }

    /**
     * Set the value of Tracks
     *
     * @param TrackBase track
     *
     * @return self
     */
    public function addTrack(TrackBase $track)
    {
        $this->tracks[] = $track;
        return $this;
    }

    /**
     * Set the value of Tracks
     *
     * @param Track track
     *
     * @return self
     */
    public function removeTrack(Track $track)
    {
        $this->tracks->removeElement($track);
        return $this;
    }


    /**
     * Get the value of band
     *
     * @return mixed
     */
    public function getBand()
    {
        return $this->band;
    }

    /**
     * Set the value of band
     *
     * @param mixed band
     *
     * @return Project
     */
    public function setBand($band)
    {
        $this->band = $band;

        return $this;
    }
}
