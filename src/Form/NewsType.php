<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class NewsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null,
                [
                    'label' => 'Nom',
                    'constraints' => [
                        new Assert\NotBlank([
                            'message' => 'Ce champs est obligatoire'
                        ])
                    ]
                ]
            )
            ->add('message', TextareaType::class,
                [
                    'label' => 'Message',
                    'constraints' => [
                        new Assert\NotBlank([
                            'message' => 'Ce champs est obligatoire'
                        ])
                    ],
                    'attr' => ['class' => 'tinymce']
                ]
            )
            ->add('type', ChoiceType::class,
                [
                    'choices'  => [
                        'News' => 'news',
                        'PatchNote' => 'patchnote',
                        'Mise en avant' => 'mea',
                    ],
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\News',
            'attr' => ['novalidate' => 'novalidate'],
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_news';
    }


}
