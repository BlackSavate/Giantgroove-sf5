<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class TrackType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null,
                [
                    'label' => 'Nom',
                    'constraints' => [
                        new Assert\NotBlank([
                            'message' => 'Ce champs est obligatoire'
                        ])
                    ]
                ])
            ->add('is_open', CheckboxType::class, [
                    'label' => 'Ouvert aux contributions'
                ])
            ->add('audio', FileType::class,
                [
                    'label' => 'Audio',
                    'data_class' => null,
                ])
            ->add('instruments', null,
                [
                    'label' => 'Instruments',
                    'expanded' => false,
                ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Track',
            'attr' => ['novalidate' => 'novalidate'],
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_track';
    }


}
