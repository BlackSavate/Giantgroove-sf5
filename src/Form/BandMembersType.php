<?php

namespace App\Form;

use App\Entity\Band;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BandMembersType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $band = $options['data'];
        $builder
            ->add('manager', ChoiceType::class,
                [
                    'label' => 'New Manager',
                    'choices' => $this->getBandMembers($band),
                    'required' => true,
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Band',
            'attr' => array('novalidate' => 'novalidate'),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_band';
    }

    private function getBandMembers(Band $band)
    {
        $managerChoices = [];
        $members = $band->getMembers();
        foreach ($members as $member) {
            if($member !== $band->getManager()) {
                $managerChoices[$member->getUsername()] = $member;
            }
        }
        return $managerChoices;
    }
}
