<?php

namespace App\Form;

use App\Entity\Band;
use App\Entity\Style;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints as Assert;

class ProjectType extends AbstractType
{
    private Security $security;
    private EntityManagerInterface $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null,
            [
                'label' => 'Titre',
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Ce champs est obligatoire'
                    ]),
                ]
            ])
            ->add('band', ChoiceType::class,
                [
                    'placeholder' => '- Aucun -',
                    'label' => 'Associer au groupe',
                    'choices' => $this->getBands(),
                    'required' => false,
                ]
            )
//            ->add('style', ChoiceType::class,
//            [
//                'label' => 'Styles',
//                'choices' => $this->getStyles(),
//                'expanded' => true,
//                'multiple' => true
//            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Project',
            'attr' => array('novalidate' => 'novalidate'),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_project';
    }


    private function getBands()
    {
        $user = $this->security->getUser();
        $bandChoices = [];
        if($user instanceof User) {
            $bands = $user->getBands();
            foreach ($bands as $band) {
                $bandChoices[$band->getName()] = $band;
            }
        }
        return $bandChoices;
    }
    private function getStyles()
    {
        $stylesChoices = [];
        $styleRepo = $this->entityManager->getRepository(Style::class);
        $styles = $styleRepo->findAll();
            foreach ($styles as $style) {
                $stylesChoices[$style->getName()] = $style;
            }

        return $stylesChoices;
    }
}
