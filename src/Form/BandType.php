<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class BandType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null,
                [
                    'label' => 'Nom',
                ]
            )
            ->add('description', TextareaType::class,
                [
                    'label' => 'Description',
                    'attr' => ['class' => 'tinymce']
                ]
            )
            ->add('logo', FileType::class, [
                    'label' => 'Logo',
                    'data_class' => null,
                    'required' => false
                ]
            )
            ->add('banner', FileType::class, [
                    'label' => 'Bannière',
                    'data_class' => null,
                    'required' => false
                ]
            )
        ;

        if ($options['is_update_form']) {
            $builder
                ->add('remove_logo', CheckboxType::class, [
                    'required' => false,
                    'mapped' => false, // Do not map this field to the entity
                ])
                ->add('remove_banner', CheckboxType::class, [
                    'required' => false,
                    'mapped' => false, // Do not map this field to the entity
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Band',
            'attr' => array('novalidate' => 'novalidate'),
            'is_update_form' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_band';
    }


}
