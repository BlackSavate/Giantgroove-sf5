<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Form\ProjectType;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\MultiMatch;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use function PHPUnit\Framework\throwException;

/**
 * @Route("/project", name="project_")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 * @return Response
 */
class ProjectController extends BaseController {

    /**
     * @Route("/", name="list", methods={"GET"})
     * @return Response
     */
    public function list(): Response {
        $em = $this->doctrine->getManager();
        $projectRepo = $em->getRepository(Project::class);
        $projects = $projectRepo->findAll();
        $userBandIds = $this->getUserBandIds();

        foreach ($projects as $key => $project) {
            if($project->getBand() !== null && !in_array($project->getBand()->getId(), $userBandIds)) {
                unset($projects[$key]);
            }
        }

        return $this->render("default/project/list.html.twig", [
            'list' => $projects
        ]);
    }

    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function create(Request $request): Response {
        return parent::create($request);
    }

    /**
     * @Route("/{slug}", name="detail", methods={"GET"})
     * @param string $slug
     * @return Response
     */
    public function detail(string $slug): Response {
        $em = $this->doctrine->getManager();
        $projectRepo = $em->getRepository(Project::class);
        $project = $projectRepo->findOneBySlug($slug);

        if (!$project instanceof Project) {
            return new Response(null, 404);
        }

        if($project->getBand()) {
            $userBandIds = $this->getUserBandIds();
            if(!in_array($project->getBand()->getId(), $userBandIds)) {
                throw $this->createAccessDeniedException("L'accès à ce projet est réservé aux membres d'un groupe.");
            }
        }
        return $this->render('default/project/detail.html.twig', [
            'project' => $project,
            'tracks' => $project->getTracks()
        ]);
    }

    /**
     * @Route("/{slug}/edit", name="update", methods={"GET", "POST"})
     * @param string $slug
     * @param Request $request
     * @return Response
     */
    public function update(string $slug, Request $request): Response {
        $em = $this->doctrine->getManager();

        $projectRepo = $em->getRepository(Project::class);
        $project = $projectRepo->findOneBySlug($slug);

        if (!$project instanceof Project) {
            return new Response(null, 404);
        }

        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project = $form->getData();
            $project->setTitle($project->getTitle());
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('project_detail', [
                'slug' => $project->getSlug()
            ]);
        }

        return $this->render('default/project/update.html.twig', [
            'project' => $project,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/search", name="search", methods={"GET"}, priority=10)
     * @param Request $request
     * @return Response
     */
    public function search(Request $request): Response {
        return parent::search($request);
    }


    public function getUserBandIds() {
        $user = $this->getUser();
        $userBandIds = [];
        if($user instanceof User) {
            $userBands = $user->getBands();
            $userBandIds = [];
            foreach ($userBands as $band) {
                $userBandIds[] = $band->getId();
            }
        }
        return $userBandIds;
    }

}
