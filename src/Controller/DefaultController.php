<?php

namespace App\Controller;

use App\Entity\Band;
use App\Entity\News;
use App\Entity\Project;
use Doctrine\Persistence\ObjectManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends BaseController {
    /**
     * @Route("/", name="homepage")
     * @return Response
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function homepage(): Response {
        $random = [];
        /** @var ObjectManager $em */
        $em = $this->doctrine->getManager();

        $newsRepo = $em->getRepository(News::class);
        $news = $newsRepo->findBy([], ['created_at' => 'DESC']);

//        foreach ($news as $newsItem) {
//            $newsItem->setMessage(nl2br($newsItem->getMessage()));
//        }

        $projectRepo = $em->getRepository(Project::class);
        $random['project'] = $projectRepo->getRandom(15);
        $bandRepo = $em->getRepository(Band::class);
        $random['band'] = $bandRepo->getRandom(15);

        return $this->render('homepage.html.twig', [
            'news' => $news,
            'random' => $random
        ]);
    }
}
