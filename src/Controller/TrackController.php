<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Track;
use App\Form\TrackType;
use DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/{project}/track", name="track_")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 * @return Response
 */
class TrackController extends BaseController {
    /**
     * Creates a new track project.
     *
     * @Route("/", name="create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response {
        $project = $request->attributes->get('project');
        $em = $this->doctrine->getManager();
        $projectRepo = $em->getRepository(Project::class);
        $project = $projectRepo->findOneBySlug($project);
        $track = new Track();
        $form = $this->createForm(TrackType::class, $track);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->doctrine->getManager();
            $em->persist($track);
            $user = $this->getUser();
            $audio = $track->getAudio();
            if (!$track->getIsOpen()) {
                if (null != $audio) {
                    $fileName = $this->generateUniqueFileName() . '.' . $audio->guessExtension();
                    $audio->move(
                        $this->getParameter('project_audio_directory') . '/' . $project->getId() . '-' . $project->getSlug(),
                        $fileName
                    );
                    $track->setAudio($fileName);
                } else {
                    $this->addFlash('error', 'La piste doit soit être ouverte aux contributions, soit comporter un fichier audio valide');
                    return $this->render('default/track/create.html.twig', array(
                        'track' => $track,
                        'project' => $project,
                        'form' => $form->createView(),
                    ));
                }
            } else {
                $track->setAudio(null);
            }

            $track->setSlug($track->getName());
            $track->setProject($project);
            $track->setAuthor($user);
            $track->setStartTime(0);
            $track->setCreatedAt(new DateTime());
            $track->setUpdatedAt(new DateTime());
            $em->flush();

            return $this->redirectToRoute('project_detail', array('slug' => $project->getSlug()));
        }

        return $this->render('default/track/create.html.twig', array(
            'track' => $track,
            'project' => $project,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{trackId}/edit", name="update", methods={"GET", "POST"})
     * @param int $track
     * @param Request $request
     * @return Response
     */
    public function update(int $trackId, Request $request): Response {
        $em = $this->doctrine->getManager();

        $track = $em->getRepository(Track::class)->find($trackId);

        if (!$track instanceof Track) {
            return new Response(null, 404);
        }

        $oldAudio = $track->getAudio();
        $project = $track->getProject();

        $form = $this->createForm(TrackType::class, $track);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $trackData = $form->getData();
            $audio = $trackData->getAudio();
            if (!$trackData->getIsOpen()) {
                if (null != $audio) {
                    $fileName = $this->generateUniqueFileName() . '.' . $audio->guessExtension();
                    $audio->move(
                        $this->getParameter('project_audio_directory') . '/' . $project->getId() . '-' . $project->getSlug(),
                        $fileName
                    );
                    $track->setAudio($fileName);
                    if($oldAudio) {
                        unlink($this->getParameter('project_audio_directory') . '/' . $project->getId() . '-' . $project->getSlug().'/'.$oldAudio);
                    }
                }
                elseif(!$oldAudio) {
                    $this->addFlash('error', 'La piste doit soit être ouverte aux contributions, soit comporter un fichier audio valide');
                    return $this->render('default/track/update.html.twig', array(
                        'track' => $track,
                        'project' => $project,
                        'form' => $form->createView(),
                    ));
                }
                else {
                    $track->setAudio($oldAudio);
                }
            // ouvert aux contribs
            } else {
                if($oldAudio) {
                    unlink($this->getParameter('project_audio_directory') . '/' . $project->getId() . '-' . $project->getSlug().'/'.$oldAudio);
                }
                $track->setAudio(null);
            }
            $em->persist($track);
            $em->flush();

            return $this->redirectToRoute('project_detail', array('slug' => $project->getSlug()));
        }

        return $this->render('default/track/update.html.twig', [
            'project' => $project,
            'track' => $track,
            'form' => $form->createView()
        ]);
    }

    /**
     * Deletes a track entity.
     *
     * @Route("/{track}", name="delete", methods={"DELETE"})
     * @ParamConverter("track", class="App:Track")
     */
    public function delete(Track $track): RedirectResponse {
        $em = $this->doctrine->getManager();
        $project = $track->getProject();
        if(!empty($track->getContributions())) {
            $this->addFlash('error', 'Cette piste ne peut pas être supprimée car elle possède une ou plusieurs contribution(s).');
        }
        else {
            $em->remove($track);

            if (!$track->getIsOpen()) {
                $fileName = $track->getAudio();
                unlink($this->getParameter('project_audio_directory') . '/' . $project->getId() . '-' . $project->getSlug().'/'.$fileName);
            }
            $em->flush();
        }
        return $this->redirectToRoute('project_detail', array('slug' => $project->getSlug()));
    }

}
