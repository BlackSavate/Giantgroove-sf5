<?php


namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordFormType;
use App\Form\UserType;
use App\Service\FileUploader;
use Datetime;
use Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/user", name="user_")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 * @return Response
 */
class UserController extends BaseController
{
    /**
     * @Route("/edit", name="update", methods={"GET", "POST"})
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     * @throws Exception
     */
    public function update(Request $request, FileUploader $fileUploader): Response {
      $em = $this->doctrine->getManager();

      $userRepo = $em->getRepository(User::class);
      $user = $this->getUser();
        if(!$user instanceof User) {
            return new Response(null, 404);
        }
        $oldAvatar = $user->getAvatar();

//      $oldAvatar = $user->getAvatar();
      $form = $this->createForm(UserType::class, $user, ['type' => 'update']);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $avatar = $form['avatar']->getData();
          if($avatar) {

              $avatarFilename = $fileUploader->upload($avatar, 'avatar');
              $user->setAvatar($avatarFilename);
              $avatarFolder = $fileUploader->getTargetDirectory('avatar');
              unlink("$avatarFolder/$oldAvatar");
          }

          $currentDate = new Datetime();
          $user = $form->getData();
          $user->setSlug($user->getUsername());
          $user->setUpdatedAt($currentDate);
          $user->setIsActive(true);
          $em->persist($user);
          $em->flush();

          return $this->redirectToRoute('user_detail', [
            'slug' => $user->getSlug()
          ]);
      }

      return $this->render('default/user/update.html.twig', [
          'user' => $user,
          'form' => $form->createView()
      ]);
    }

    /**
     * @Route("/password", name="update_password", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function updatePassword(Request $request, UserPasswordHasherInterface $hasher): Response {
        $em = $this->doctrine->getManager();

        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $formData = $form->getData();
            $oldPassword = $formData['old_password'];
            $newPassword = $formData['password'];

            if (!$hasher->isPasswordValid($user, $oldPassword)) {
                $form->get('old_password')->addError(new FormError('Invalid old password.'));
            } else {
                $hashed = $hasher->hashPassword($user, $newPassword);
                $user->setPassword($hashed);
                $user->setUpdatedAt($currentDate);
                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('user_detail', [
                    'slug' => $user->getSlug()
                ]);
            }
        }

        return $this->render('default/user/update_password.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{slug}", name="detail", methods={"GET"})
     * @param string $slug
     * @return Response
     */
    public function detail(string $slug): Response {
        $em = $this->doctrine->getManager();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneBySlug($slug);

        if (!$user instanceof User) {
            return new Response(null, 404);
        }

        return $this->render('default/user/detail.html.twig', [
            'user' => $user
        ]);
    }
}
