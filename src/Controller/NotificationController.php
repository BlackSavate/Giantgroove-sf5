<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\User;
use App\Form\NotificationType;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/notifications", name="notifications_")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 * @return Response
 */
class NotificationController extends BaseController {

    /**
     * @Route("/", name="list", methods={"GET"})
     * @return Response
     */
    public function list(): Response {
        $em = $this->doctrine->getManager();
        $user = $this->getUser();

        $notifications = $em->getRepository(Notification::class)->findByRecipient($user, ['createdAt' => 'DESC']);

        return $this->render('default/notifications/list.html.twig', [
            'notifications' => $notifications,
        ]);
    }

    /**
     * Send new notification.
     *
     * @Route("/{to}/send", name="send", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function sendMessage(Request $request, string $to): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $notification = new Notification();
        $form = $this->createForm(NotificationType::class, $notification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->doctrine->getManager();
            $em->persist($notification);
            $notification->setCreatedAt(new Datetime);
            $userRepository = $em->getRepository(User::class);
            $recipient = $userRepository->findOneBySlug($to);

            if (!$user instanceof User || !$recipient instanceof User) {
                return new Response(null, 404);
            }

            $notification->setSender($user);
            $notification->setRecipient($recipient);
            $em->flush();
            return $this->redirectToRoute('user_detail', array('slug' => $recipient->getSlug()));
        }

        return $this->render('default/notifications/create.html.twig', array(
            'notification' => $notification,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/read/{id}", name="mark_read", methods={"POST"})
     * @return Response
     */
    public function markAsRead(string $id): Response {
        $em = $this->doctrine->getManager();
        $notificationRepositoy = $em->getRepository(Notification::class);

        $notification = $notificationRepositoy->findOneById($id);
        $notification->setStatus('read');
        $em->persist($notification);
        $em->flush();

        return new Response();
    }

    /**
     * @Route("/unread/{id}", name="mark_unread", methods={"POST"})
     * @return Response
     */
    public function markAsUnRead(string $id): Response {
        $em = $this->doctrine->getManager();
        $notificationRepositoy = $em->getRepository(Notification::class);

        $notification = $notificationRepositoy->findOneById($id);
        $notification->setStatus('unread');
        $em->persist($notification);
        $em->flush();


        return new Response();
    }


    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     * @return Response
     */
    public function delete(string $id): Response {
        $em = $this->doctrine->getManager();
        $notificationRepositoy = $em->getRepository(Notification::class);

        $notification = $notificationRepositoy->findOneById($id);
        $em->remove($notification);
        $em->flush();


        return new Response();
    }

}
