<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Datetime;
use Exception;
use RuntimeException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends BaseController
{
    /**
     * @Route("/login", name="login", methods={"GET", "POST"})
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response {
      // get the login error if there is one
      $error = $authenticationUtils->getLastAuthenticationError();

      // last username entered by the user
      $lastUsername = $authenticationUtils->getLastUsername();

      return $this->render('security/login.html.twig', array(
        'last_username' => $lastUsername,
        'error'         => $error,
      ));
    }

    /**
     * @Route("/register", name="register", methods={"GET", "POST"})
     * @param Request $request
     * @param UserPasswordHasherInterface $hasher
     * @return Response
     * @throws Exception
     */
    public function register(Request $request, UserPasswordHasherInterface $hasher): Response {
        $em = $this->doctrine->getManager();

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $currentDate = new Datetime();
            $user = $form->getData();
            $user->setSlug($user->getUsername());
            $user->setCreatedAt($currentDate);
            $user->setUpdatedAt($currentDate);
            $user->setIsActive(true);
            $hashed = $hasher->hashPassword($user, $user->getPassword());
            $user->setPassword($hashed);
            $avatar = $user->getAvatar();
            if (null != $avatar) {
                $fileName = $this->generateUniqueFileName().'.'.$avatar->guessExtension();
                $avatar->move(
                    $this->getParameter('avatar_directory'),
                    $fileName
                );
                $user->setAvatar($fileName);
            }
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/logout", name="logout", methods={"GET"})
     * @throws Exception
     */
    public function logout(): void {
        // controller can be blank: it will never be executed!
        throw new RuntimeException('Don\'t forget to activate logout in security.yaml');
    }

}
