<?php

namespace App\Controller\Admin;

use Datetime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\User;
use App\Form\UserType;

/**
 * @Route("/admin/user", name="admin_user_")
 * @IsGranted("ROLE_ADMIN")
 */
class UserController extends AdminBaseController {
    /**
     * @Route("/", name="list", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function list(): Response {
        return parent::list();
    }

    /**
     * @Route("/{slug}", name="update", methods={"GET", "POST"})
     * @param Request $request
     * @param string $slug
     * @return Response
     * @throws \Exception
     */
    public function update(Request $request, string $slug): Response {
        $em = $this->doctrine->getManager();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneBySlug($slug);

        $form = $this->createForm(UserType::class, $user, ['type' => 'update']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $user = $form->getData();
            $user->setSlug($user->getUsername());
            $user->setUpdatedAt($currentDate);
            $em->persist($user);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'update', 'user', $user->getId(), $user->getUsername());

            $userList = $userRepo->findAll();
            return $this->redirectToRoute('admin_user_list', [
                'userList' => $userList
            ]);
        }

        return $this->render('admin/user/update.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{slug}/enable", name="enable", methods={"GET"})
     * @param string $slug
     * @return Response
     */
    public function enable(string $slug): Response {
        $em = $this->doctrine->getManager();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneBySlug($slug);

        if (!$user instanceof User) {
            return new Response(null, 404);
        }

        $user->setIsActive(!$user->getIsActive());
        $em->persist($user);
        $em->flush();

        $user->getIsActive()
        ? $this->adminHistoryService->addHistory($this->getUser(), 'enable', 'user', $user->getId(), $user->getUsername())
        : $this->adminHistoryService->addHistory($this->getUser(), 'disable', 'user', $user->getId(), $user->getUsername());


        $userList = $userRepo->findAll();

        return $this->redirectToRoute('admin_user_list', [
            'userList' => $userList
        ]);
    }

    /**
     * @Route("/{slug}/admin", name="admin", methods={"GET"})
     * @param string $slug
     * @return Response
     */
    public function admin(string $slug): Response {
        $em = $this->doctrine->getManager();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneBySlug($slug);

        if (!$user instanceof User) {
            return new Response(null, 404);
        }

        $roles = $user->getRoles() ?? [];
        $hasAdminRole = array_search('ROLE_ADMIN', $roles);
        if($hasAdminRole) {
            unset($roles[$hasAdminRole]);
        }
        else {
            $roles[] = 'ROLE_ADMIN';
        }

        $user->setRoles($roles);
        $em->persist($user);
        $em->flush();

        $hasAdminRole
            ? $this->adminHistoryService->addHistory($this->getUser(), 'remove_admin', 'user', $user->getId(), $user->getUsername())
            : $this->adminHistoryService->addHistory($this->getUser(), 'add_admin', 'user', $user->getId(), $user->getUsername());

        $userList = $userRepo->findAll();

        return $this->redirectToRoute('admin_user_list', [
            'userList' => $userList
        ]);
    }
}
