<?php

namespace App\Controller\Admin;

use Datetime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Entity\Style;
use App\Form\StyleType;

/**
 * @Route("/admin/style", name="admin_style_")
 * @IsGranted("ROLE_ADMIN")
 */
class StyleController extends AdminBaseController {
    /**
     * @Route("/", name="list", methods={"GET"})
     * @return Response
     */
    public function list(): Response {
        return parent::list();
    }

    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response {
        $em = $this->doctrine->getManager();
        $styleRepo = $em->getRepository(Style::class);

        $form = $this->createForm(StyleType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $style = $form->getData();
            $style->setSlug($style->getName());
            $style->setCreatedAt($currentDate);
            $style->setUpdatedAt($currentDate);
            $em->persist($style);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'create', 'style', $style->getId(), $style->getName());

            $styleList = $styleRepo->findAll();
            return $this->redirectToRoute('admin_style_list', [
                'styleList' => $styleList
            ]);
        }

        return $this->render('admin/style/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{slug}", name="detail", methods={"GET"})
     * @param string $slug
     * @return Response
     */
    public function detail(string $slug): Response {
        $em = $this->doctrine->getManager();
        $styleRepo = $em->getRepository(Style::class);
        $style = $styleRepo->findOneBySlug($slug);

        if (!$style instanceof Style) {
            return new Response(null, 404);
        }

        return $this->render('admin/style/detail.html.twig', [
            'style' => $style,
        ]);
    }

    /**
     * @Route("/{slug}/edit", name="update", methods={"GET", "POST"})
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function update(Request $request, string $slug): Response {
        $em = $this->doctrine->getManager();
        $styleRepo = $em->getRepository(Style::class);
        $style = $styleRepo->findOneBySlug($slug);

        $form = $this->createForm(StyleType::class, $style);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $style = $form->getData();
            $style->setSlug($style->getName());
            $style->setUpdatedAt($currentDate);
            $em->persist($style);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'update', 'style', $style->getId(), $style->getName());

            $styleList = $styleRepo->findAll();
            return $this->redirectToRoute('admin_style_list', [
                'styleList' => $styleList
            ]);
        }

        return $this->render('admin/style/update.html.twig', [
            'style' => $style,
            'form' => $form->createView()
        ]);
    }

    /**
     * Deletes a style entity.
     *
     * @Route("/{style}", name="delete", methods={"DELETE"})
     * @ParamConverter("style", class="App:Style")
     */
    public function delete(Style $style): RedirectResponse {
        $styleId = $style->getId();
        $styleName = $style->getName();
        $em = $this->doctrine->getManager();
        $em->remove($style);
        $em->flush();
        $this->adminHistoryService->addHistory($this->getUser(), 'delete', 'style', $styleId, $styleName);

        return $this->redirectToRoute('admin_style_list');
    }
}
