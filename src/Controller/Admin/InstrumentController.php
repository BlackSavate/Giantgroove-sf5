<?php

namespace App\Controller\Admin;

use Datetime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Entity\Instrument;
use App\Form\InstrumentType;

/**
 * @Route("/admin/instrument", name="admin_instrument_")
 * @IsGranted("ROLE_ADMIN")
 */
class InstrumentController extends AdminBaseController {
    /**
     * @Route("/", name="list", methods={"GET"})
     * @return Response
     */
    public function list(): Response {
        return parent::list();
    }

    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response {
        $em = $this->doctrine->getManager();
        $instrumentRepo = $em->getRepository(Instrument::class);

        $form = $this->createForm(InstrumentType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $instrument = $form->getData();
            $instrument->setSlug($instrument->getName());
            $instrument->setCreatedAt($currentDate);
            $instrument->setUpdatedAt($currentDate);
            $em->persist($instrument);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'create', 'instrument', $instrument->getId(), $instrument->getName());

            $instrumentList = $instrumentRepo->findAll();
            return $this->redirectToRoute('admin_instrument_list', [
                'instrumentList' => $instrumentList
            ]);
        }

        return $this->render('admin/instrument/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{slug}", name="detail", methods={"GET"})
     * @param string $slug
     * @return Response
     */
    public function detail(string $slug): Response {
        $em = $this->doctrine->getManager();
        $instrumentRepo = $em->getRepository(Instrument::class);
        $instrument = $instrumentRepo->findOneBySlug($slug);

        if (!$instrument instanceof Instrument) {
            return new Response(null, 404);
        }

        return $this->render('admin/instrument/detail.html.twig', [
            'instrument' => $instrument,
        ]);
    }

    /**
     * @Route("/{slug}/edit", name="update", methods={"GET", "POST"})
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function update(Request $request, string $slug): Response {
        $em = $this->doctrine->getManager();
        $instrumentRepo = $em->getRepository(Instrument::class);
        $instrument = $instrumentRepo->findOneBySlug($slug);

        $form = $this->createForm(InstrumentType::class, $instrument);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $instrument = $form->getData();
            $instrument->setSlug($instrument->getName());
            $instrument->setUpdatedAt($currentDate);
            $em->persist($instrument);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'update', 'instrument', $instrument->getId(), $instrument->getName());

            $instrumentList = $instrumentRepo->findAll();
            return $this->redirectToRoute('admin_instrument_list', [
                'instrumentList' => $instrumentList
            ]);
        }

        return $this->render('admin/instrument/update.html.twig', [
            'instrument' => $instrument,
            'form' => $form->createView()
        ]);
    }

    /**
     * Deletes a instrument entity.
     *
     * @Route("/{instrument}", name="delete", methods={"DELETE"})
     * @ParamConverter("instrument", class="App:Instrument")
     */
    public function delete(Instrument $instrument): RedirectResponse {
        $instrumentId = $instrument->getId();
        $instrumentName = $instrument->getName();
        $em = $this->doctrine->getManager();
        $em->remove($instrument);
        $em->flush();

        $this->adminHistoryService->addHistory($this->getUser(), 'delete', 'instrument', $instrumentId, $instrumentName);
        return $this->redirectToRoute('admin_instrument_list');
    }
}
