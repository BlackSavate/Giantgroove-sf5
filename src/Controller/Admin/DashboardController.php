<?php

namespace App\Controller\Admin;

use App\Entity\AdminHistory;
use App\Entity\Band;
use App\Entity\Instrument;
use App\Entity\News;
use App\Entity\Project;
use App\Entity\Style;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/dashboard", name="admin_dashboard_")
 */
class DashboardController extends AdminBaseController {
    /**
     * @Route("/", name="dashboard")
     * @return Response
     */
    public function dashboard(): Response {
        return $this->render('admin/dashboard.html.twig');
    }

    /**
     * @Route("/recent_activity", name="recent_activity")
     * @return Response
     */
    public function recentActivity(Request $request): Response {
        $type = $request->query->get('type') ?? 'user';
        $nbItems = $request->query->get('nbItems') ?? 10;
        $em = $this->doctrine->getManager();
        $parameters = [];
        switch ($type) {
            case 'user':
                $entity = User::class;
                $parameters = ['active_only' => $request->query->get('active_only') ?? true];
                break;
            case 'project':
                $entity = Project::class;
                break;
            case 'band':
                $entity = Band::class;
                break;
            case 'style':
                $entity = Style::class;
                break;
            case 'instrument':
                $entity = Instrument::class;
                break;
            case 'news':
                $entity = News::class;
                break;
            default:
                $entity = User::class;
                $parameters = ['active_only' => $request->query->get('active_only') ?? true];
        }
        $repo = $em->getRepository($entity);
        $lastItems = $repo->getLastItems($nbItems, $parameters);
        return $this->render('admin/dashboard.html.twig', ['type' => $type, 'lastItems' => $lastItems]);
    }

    /**
     * @Route("/stats", name="stats")
     * @return Response
     */
    public function stats(): Response {
        $countUser = $this->doctrine->getManager()->getRepository(User::class)->countItems();
        $countBand = $this->doctrine->getManager()->getRepository(Band::class)->countItems();
        $countProject = $this->doctrine->getManager()->getRepository(Project::class)->countItems();
        return $this->render('admin/dashboard.html.twig', [
            'countUser' => $countUser,
            'countBand' => $countBand,
            'countProject' => $countProject,
        ]);
    }

    /**
     * @Route("/history", name="history")
     * @return Response
     */
    public function history(): Response {
        $historyRepository = $this->doctrine->getManager()->getRepository(AdminHistory::class);
        $order = (isset($_GET["orderBy"]) && isset($_GET["orientation"])) ? [$_GET["orderBy"] => $_GET["orientation"]] : ['time' => 'DESC'];
        $subParam = explode('.', array_key_first($order));
        $nbItems = $_GET["nbItems"] ?? 10;
        $list = (count($subParam) > 1) ? $historyRepository->findByJoinField($subParam, $order) : $list = $historyRepository->findBy([], $order);
        $filteredItems = [];
        $action = $_GET["action"] ?? "all";
        $type = $_GET["type"] ?? "all";
        $userId = $_GET["userId"] ?? "all";
        foreach ($list as $item) {
            if (($action === $item->getAction() || $action === 'all') && ($type === $item->getTargetType() || $type === 'all') && ($userId == $item->getUserId() || $userId === 'all')) {
                $exclude = false;
                if (isset($_GET["start"]) && !empty($_GET["start"]) && strtotime($_GET['start']) > $item->getTime()->getTimestamp() ) {
                    $exclude = true;
                }
                if (isset($_GET["end"]) && !empty($_GET["end"]) && strtotime($_GET['end']) < $item->getTime()->getTimestamp() ) {
                    $exclude = true;
                }
                if (isset($_GET["userId"]) && !empty($_GET["userId"]) && $_GET["userId"] != $item->getUserId() && $_GET["userId"] != 'all' ) {
                    $exclude = true;
                }
                if($exclude === false) {
                    $filteredItems[] = $this->adminHistoryService->getMessage($item);
                }
            }
        }
        $page = isset($_GET["page"]) ? $_GET["page"] : 1;
        $basePath = 'admin/' . strtolower($this->getEntityName());
        $params = [];
        if(isset($_GET["start"]) && !empty($_GET["start"])) {
            $params['start'] = $_GET["start"];
        }
        if(isset($_GET["end"]) && !empty($_GET["end"])) {
            $params['end'] = $_GET["end"];
        }
        if(isset($_GET["userId"]) && !empty($_GET["userId"])) {
            $params['userId'] = $_GET["userId"];
        }
        $paginatedResults = $historyRepository->getPaginated($filteredItems, $nbItems, $page, '/' . $basePath . '/history', $order, $params);
        $userRepository = $this->doctrine->getManager()->getRepository(User::class);
        $adminUsers = $userRepository->findUsersByRole('ROLE_ADMIN');

        return $this->render('admin/dashboard.html.twig', [
                'history' => $paginatedResults['results'],
                'pager' => $paginatedResults['count'] > 10 ? $paginatedResults['pager'] : null,
                'adminUsers' => $adminUsers]
        );
    }

    /**
     * @Route("/other", name="other")
     * @return Response
     */
    public function other(): Response {
        return $this->render('admin/dashboard.html.twig');
    }
}
