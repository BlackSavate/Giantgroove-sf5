<?php

namespace App\Controller\Admin;

use Datetime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Entity\News;
use App\Form\NewsType;

/**
 * @Route("/admin/news", name="admin_news_")
 * @IsGranted("ROLE_ADMIN")
 */
class NewsController extends AdminBaseController {
    /**
     * @Route("/", name="list", methods={"GET"})
     * @return Response
     */
    public function list(): Response {
        return parent::list();
    }

    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response {
        $em = $this->doctrine->getManager();
        $newsRepo = $em->getRepository(News::class);

        $form = $this->createForm(NewsType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $news = $form->getData();

            $lastCounterOfType = $newsRepo->findLastItemOfType($news->getType());
            ($lastCounterOfType !== null) ? $news->setCounter($lastCounterOfType->getCounter()+1) : $news->setCounter(1);

            $news->setSlug($news->getTitle());
            $news->setCreatedAt($currentDate);
            $news->setUpdatedAt($currentDate);
            $em->persist($news);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'create', 'news', $news->getId(), $news->getTitle());

            $newsList = $newsRepo->findAll();
            return $this->redirectToRoute('admin_news_list', [
                'newsList' => $newsList
            ]);
        }

        return $this->render('admin/news/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{slug}", name="detail", methods={"GET"})
     * @param string $slug
     * @return Response
     */
    public function detail(string $slug): Response {
        $em = $this->doctrine->getManager();
        $newsRepo = $em->getRepository(News::class);
        $news = $newsRepo->findOneBySlug($slug);

        if (!$news instanceof News) {
            return new Response(null, 404);
        }

        return $this->render('admin/news/detail.html.twig', [
            'news' => $news,
        ]);
    }


    /**
     * @Route("/{slug}/edit", name="update", methods={"GET", "POST"})
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function update(Request $request, string $slug): Response {
        $em = $this->doctrine->getManager();
        $newsRepo = $em->getRepository(News::class);
        $news = $newsRepo->findOneBySlug($slug);

        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $news = $form->getData();
            $news->setSlug($news->getTitle());
            $news->setUpdatedAt($currentDate);
            $em->persist($news);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'update', 'news', $news->getId(), $news->getTitle());

            $newsList = $newsRepo->findAll();
            return $this->redirectToRoute('admin_news_list', [
                'newsList' => $newsList
            ]);
        }

        return $this->render('admin/news/update.html.twig', [
            'news' => $news,
            'form' => $form->createView()
        ]);
    }

    /**
     * Deletes a news entity.
     *
     * @Route("/{news}", name="delete", methods={"DELETE"})
     * @ParamConverter("news", class="App:News")
     */
    public function delete(News $news): RedirectResponse {
        $newsId = $news->getId();
        $title = $news->getTitle();
        $em = $this->doctrine->getManager();
        $em->remove($news);
        $em->flush();
        $this->adminHistoryService->addHistory($this->getUser(), 'delete', 'news', $newsId, $title);

        return $this->redirectToRoute('admin_news_list');
    }
}
