<?php

namespace App\Controller\Admin;

use App\Entity\Band;
use App\Form\BandType;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

;

/**
 * @Route("/admin/band", name="admin_band_")
 * @IsGranted("ROLE_ADMIN")
 */
class BandController extends AdminBaseController {
    /**
     * @Route("/", name="list", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function list(): Response {
        return parent::list();
    }

    /**
     * @Route("/{slug}/edit", name="update", methods={"GET", "POST"})
     * @param string $slug
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     * @throws \Exception
     */
    public function update(string $slug, Request $request, FileUploader $fileUploader): Response {
        $em = $this->doctrine->getManager();

        $bandRepo = $em->getRepository(Band::class);
        $band = $bandRepo->findOneBySlug($slug);
        $oldLogo = $band->getLogo();
        $oldBanner = $band->getBanner();
        if(!$band instanceof Band) {
            return new Response(null, 404);
        }

        $form = $this->createForm(BandType::class, $band, [
            'is_update_form' => true,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Check if the "remove logo" checkbox is checked
            $removeLogo = $form->get('remove_logo')->getData();
            if ($removeLogo) {
                // Remove the logo and set the logo property to null
                $logoFolder = $fileUploader->getTargetDirectory('band_logo');
                if ($oldLogo !== null) {
                    unlink("$logoFolder/$oldLogo");
                }
                $band->removeLogo();
            } else {
                // Check if a new logo file was uploaded
                $logoFile = $form['logo']->getData();
                if ($logoFile) {
                    // Upload the new logo file and set the logo property
                    $logoFilename = $fileUploader->upload($logoFile, 'band_logo');
                    $band->setLogo($logoFilename);

                    // Remove the old logo file
                    $logoFolder = $fileUploader->getTargetDirectory('band_logo');
                    if ($oldLogo !== null) {
                        unlink("$logoFolder/$oldLogo");
                    }
                }
            }
            $removeBanner = $form->get('remove_banner')->getData();
            if ($removeBanner) {
                // Remove the banenr and set the logo property to null
                $bannerFolder = $fileUploader->getTargetDirectory('band_banner');
                if ($oldBanner !== null) {
                    unlink("$bannerFolder/$oldBanner");
                }
                $band->removeBanner();
            } else {
                // Check if a new banner file was uploaded
                $bannerFile = $form['banner']->getData();
                if ($bannerFile) {
                    // Upload the new banner file and set the banner property
                    $bannerFilename = $fileUploader->upload($bannerFile, 'band_banner');
                    $band->setBanner($bannerFilename);

                    // Remove the old banner file
                    $bannerFolder = $fileUploader->getTargetDirectory('band_banner');
                    if ($oldBanner !== null) {
                        unlink("$bannerFolder/$oldBanner");
                    }
                }
            }

            $currentDate = new \Datetime();
            $band = $form->getData();
            $band->setSlug($band->getName());
            $band->setUpdatedAt($currentDate);
            $em->persist($band);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'update', 'band', $band->getId(), $band->getName());

            return $this->redirectToRoute('admin_band_list', [
                'slug' => $band->getSlug()
            ]);
        }

        return $this->render('admin/band/update.html.twig', [
            'band' => $band,
            'form' => $form->createView()
        ]);
    }
}
