<?php

namespace App\Controller\Admin;

use Datetime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Project;
use App\Form\ProjectType;

/**
 * @Route("/admin/project", name="admin_project_")
 * @IsGranted("ROLE_ADMIN")
 */
class ProjectController extends AdminBaseController {
    /**
     * @Route("/", name="list", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function list(): Response {
        return parent::list();
    }

    /**
     * @Route("/{slug}", name="update", methods={"GET", "POST"})
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function update(Request $request, string $slug): Response {
        $em = $this->doctrine->getManager();
        $projectRepo = $em->getRepository(Project::class);
        $project = $projectRepo->findOneBySlug($slug);

        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentDate = new Datetime();
            $project = $form->getData();
            $project->setSlug($project->getSlug());
            $project->setUpdatedAt($currentDate);
            $em->persist($project);
            $em->flush();

            $this->adminHistoryService->addHistory($this->getUser(), 'update', 'project', $project->getId(), $project->getTitle());

            $projectList = $projectRepo->findAll();
            return $this->redirectToRoute('admin_project_list', [
                'projectList' => $projectList
            ]);
        }

        return $this->render('admin/project/update.html.twig', [
            'project' => $project,
            'form' => $form->createView()
        ]);
    }
}
