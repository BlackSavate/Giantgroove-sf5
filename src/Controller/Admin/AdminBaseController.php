<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin", name="admin_")
 * @IsGranted("ROLE_ADMIN")
 */
abstract class AdminBaseController extends BaseController {
    /**
     * @Route("/", name="list", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function list(): Response {
        $em = $this->doctrine->getManager();
        $repo = $em->getRepository($this->getEntity());
        $order = (isset($_GET["orderBy"]) && isset($_GET["orientation"])) ? [$_GET["orderBy"] => $_GET["orientation"]] : [];
        $subParam = explode('.',array_key_first($order));
        $list = (count($subParam) > 1) ? $repo->findByJoinField($subParam, $order): $list = $repo->findBy([], $order);
        $page = isset($_GET["page"]) ? $_GET["page"] : 1;
        $basePath = 'admin/'.strtolower($this->getEntityName());
        $paginatedResults = $repo->getPaginated($list, 10, $page, '/'.$basePath, $order);
        return $this->render($basePath.'/list.html.twig', [
            'list' => $paginatedResults['results'],
            'pager' => $paginatedResults['count'] > 10 ? $paginatedResults['pager'] : null
        ]);
    }
}
