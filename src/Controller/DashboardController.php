<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Elastica\Client;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DashboardController extends BaseController {
//    protected CacheManager $cacheManager;
//    protected ManagerRegistry $doctrine;
//    protected Client $client;
//    public function __construct(CacheManager $cacheManager, ManagerRegistry $doctrine, Client $client)
//    {
//        parent::__construct($cacheManager, $doctrine, $client);
//    }

    /**
     * @Route("/dashboard", name="dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @return Response
     */
    public function dashboard(): Response {
        $user = $this->getUser();

        if (!$user instanceof User) {
            return new Response(null, 404);
        }

        return $this->render('dashboard.html.twig', [
            'user' => $user
        ]);
    }
}
