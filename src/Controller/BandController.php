<?php

namespace App\Controller;

use App\Entity\Band;
use App\Entity\Notification;
use App\Entity\User;
use App\Form\BandManagerType;
use App\Form\BandType;
use App\Form\UserType;
use App\Service\FileUploader;
use App\Service\PusherService;
use Datetime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/band", name="band_")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 * @return Response
 */
class BandController extends BaseController {
    /**
     * @Route("/", name="list", methods={"GET"})
     * @return Response
     */
    public function list(): Response {
        $em = $this->doctrine->getManager();
        $bandRepo = $em->getRepository(Band::class);
        $bandList = $bandRepo->findAll();
        return $this->render('default/band/list.html.twig', [
            'bandList' => $bandList
        ]);
    }

    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $band = new Band($user);
        $form = $this->createForm(BandType::class, $band);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->doctrine->getManager();
            $em->persist($band);
            $band->setSlug($band->getName());
            $band->setCreatedAt(new Datetime);
            $band->setUpdatedAt(new Datetime);

            $logo = $band->getLogo();
            if (null != $logo) {
                $fileName = $this->generateUniqueFileName().'.'.$logo->guessExtension();
                $logo->move(
                    $this->getParameter('band_logo_directory'), // TODO: use the fileUploader service
                    $fileName
                );
                $user->setAvatar($fileName);
            }

            if (!$user instanceof User) {
                return new Response(null, 404);
            }

            $band->setMembers([$user]);
            $band->setManager($user);
            $em->flush();
            return $this->redirectToRoute('band_detail', array('slug' => $band->getSlug()));
        }

        return $this->render('default/band/create.html.twig', array(
            'band' => $band,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/{slug}", name="detail", methods={"GET"})
     * @param string $slug
     * @return Response
     */
    public function detail(string $slug): Response {
        $em = $this->doctrine->getManager();
        $bandRepo = $em->getRepository(Band::class);
        /** @var Band $band */
        $band = $bandRepo->findOneBySlug($slug);

        if (!$band instanceof Band) {
            return new Response(null, 404);
        }

        return $this->render('default/band/detail.html.twig', [
            'band' => $band
        ]);
    }

    /**
     * @Route("/search", name="search", methods={"GET"}, priority=10)
     * @param Request $request
     * @return Response
     */
    public function search(Request $request): Response {
        return parent::search($request);
    }

    /**
     * @Route("/{slug}/edit", name="update", methods={"GET", "POST"})
     * @param string $slug
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     * @throws Exception
     */
    public function update(string $slug, Request $request, FileUploader $fileUploader): Response {
        $em = $this->doctrine->getManager();

        $bandRepo = $em->getRepository(Band::class);
        $band = $bandRepo->findOneBySlug($slug);
        $oldLogo = $band->getLogo();
        $oldBanner = $band->getBanner();
        if(!$band instanceof Band) {
            return new Response(null, 404);
        }

        $form = $this->createForm(BandType::class, $band, [
            'is_update_form' => true,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Check if the "remove logo" checkbox is checked
            $removeLogo = $form->get('remove_logo')->getData();
            if ($removeLogo) {
                // Remove the logo and set the logo property to null
                $logoFolder = $fileUploader->getTargetDirectory('band_logo');
                if ($oldLogo !== null) {
                    unlink("$logoFolder/$oldLogo");
                }
                $band->removeLogo();
            } else {
                // Check if a new logo file was uploaded
                $logoFile = $form['logo']->getData();
                if ($logoFile) {
                    // Upload the new logo file and set the logo property
                    $logoFilename = $fileUploader->upload($logoFile, 'band_logo');
                    $band->setLogo($logoFilename);

                    // Remove the old logo file
                    $logoFolder = $fileUploader->getTargetDirectory('band_logo');
                    if ($oldLogo !== null) {
                        unlink("$logoFolder/$oldLogo");
                    }
                }
            }
            $removeBanner = $form->get('remove_banner')->getData();
            if ($removeBanner) {
                // Remove the banenr and set the logo property to null
                $bannerFolder = $fileUploader->getTargetDirectory('band_banner');
                if ($oldBanner !== null) {
                    unlink("$bannerFolder/$oldBanner");
                }
                $band->removeBanner();
            } else {
                // Check if a new banner file was uploaded
                $bannerFile = $form['banner']->getData();
                if ($bannerFile) {
                    // Upload the new banner file and set the banner property
                    $bannerFilename = $fileUploader->upload($bannerFile, 'band_banner');
                    $band->setBanner($bannerFilename);

                    // Remove the old banner file
                    $bannerFolder = $fileUploader->getTargetDirectory('band_banner');
                    if ($oldBanner !== null) {
                        unlink("$bannerFolder/$oldBanner");
                    }
                }
            }

            $currentDate = new Datetime();
            $band = $form->getData();
            $band->setSlug($band->getName());
            $band->setUpdatedAt($currentDate);
            $em->persist($band);
            $em->flush();

            return $this->redirectToRoute('band_detail', [
                'slug' => $band->getSlug()
            ]);
        }

        return $this->render('default/band/update.html.twig', [
            'band' => $band,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{slug}/manager", name="update_manager", methods={"GET", "POST"})
     * @param string $slug
     * @return Response
     */
    public function updateManager(Request $request, string $slug): Response {
        $doctrine = $this->doctrine->getManager();
        $bandRepo = $doctrine->getRepository(Band::class);
        /** @var Band $band */
        $band = $bandRepo->findOneBySlug($slug);

        if (!$band instanceof Band) {
            return new Response(null, 404);
        }

        $form = $this->createForm(BandManagerType::class, $band, ['data' => $band]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->doctrine->getManager();
            $em->persist($band);
            $em->flush();
            return $this->redirectToRoute('band_detail', array('slug' => $band->getSlug()));
        }

        return $this->render('default/band/update_manager.html.twig', [
            'band' => $band,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{slug}/members/edit", name="handle_members", methods={"GET", "POST"})
     * @param string $slug
     * @return Response
     */
    public function handleMembers(Request $request, string $slug): Response {
        $em = $this->doctrine->getManager();
        $bandRepo = $em->getRepository(Band::class);
        /** @var Band $band */
        $band = $bandRepo->findOneBySlug($slug);

        if (!$band instanceof Band) {
            return new Response(null, 404);
        }

        $members = $band->getMembers();

        return $this->render('default/band/handle_members.html.twig', [
            'band' => $band,
        ]);
    }

    /**
     * @Route("/{slug}/leave", name="leave", methods={"GET", "POST"})
     * @param string $slug
     * @return Response
     */
    public function leave(Request $request, string $slug): Response {
        $em = $this->doctrine->getManager();
        $bandRepo = $em->getRepository(Band::class);
        /** @var Band $band */
        $band = $bandRepo->findOneBySlug($slug);

        if (!$band instanceof Band) {
            return new Response(null, 404);
        }

        $band->removeMember($this->getUser());

        $em->persist($band);
        $em->flush();
        return $this->render('default/band/detail.html.twig', [
            'band' => $band,
        ]);
    }

    /**
     * @Route("/{slug}/join-request", name="join_request", methods={"GET", "POST"})
     * @param string $slug
     * @return Response
     */
    public function joinRequest(Request $request, string $slug, PusherService $pusherService, UrlGeneratorInterface $urlGenerator): Response {
        $em = $this->doctrine->getManager();
        $bandRepo = $em->getRepository(Band::class);
        /** @var Band $band */
        $band = $bandRepo->findOneBySlug($slug);

        if (!$band instanceof Band) {
            return new Response(null, 404);
        }
        $recipient = $band->getManager(); // notify the band Manager

        $sender = $this->getUser();
        $senderProfilePage = $urlGenerator->generate('user_detail', ['slug' => $sender->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        $bandHandleMembersUrl = $urlGenerator->generate('band_handle_members', ['slug' => $slug], UrlGeneratorInterface::ABSOLUTE_URL);

        $message = sprintf('<a href="%s">%s</a> has requested to join %s. Please visit <a href="%s">handle members page</a> to accept or decline.',
            $senderProfilePage,
            $sender->__toString(),
            $band->getName(),
            $bandHandleMembersUrl);

        $notification = new Notification();
        $notification->setRecipient($recipient);
        $notification->setSender($this->getUser());
        $notification->setMessage($message);
        $notification->setCreatedAt(new \DateTime());

        $em->persist($notification);
        $em->flush();

        $pusherService->trigger('notifications', 'new-notification', [
            'recipient_id' => $recipient->getId(),
            'message' => $notification->getMessage(),
        ]);

        return $this->redirectToRoute('band_detail', ['slug' => $slug]);
    }
}
