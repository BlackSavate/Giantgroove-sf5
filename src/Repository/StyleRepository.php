<?php

namespace App\Repository;

use App\Entity\Style;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Style[]    findAll()
 * @method Style[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StyleRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Style::class);
    }
    public function getLastItems(int $limit, array $parameters): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('s')
            ->from(Style::class, 's')
            ->orderBy('s.created_at', 'DESC')
            ->setMaxResults($limit) // Set the maximum number of results
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Style[] Returns an array of Style objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Style
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
