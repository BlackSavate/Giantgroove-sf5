<?php

namespace App\Repository;

use App\Entity\Instrument;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Instrument[]    findAll()
 * @method Instrument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstrumentRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Instrument::class);
    }

    public function getLastItems(int $limit, array $parameters): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('i')
            ->from(Instrument::class, 'i')
            ->orderBy('i.created_at', 'DESC')
            ->setMaxResults($limit) // Set the maximum number of results
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Instrument[] Returns an array of Instrument objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Instrument
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
