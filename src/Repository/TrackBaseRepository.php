<?php

namespace App\Repository;

use App\Entity\TrackBase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrackBase[]    findAll()
 * @method TrackBase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrackBaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrackBase::class);
    }

}
