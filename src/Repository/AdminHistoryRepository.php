<?php

namespace App\Repository;

use App\Entity\AdminHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdminHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminHistory[]    findAll()
 * @method AdminHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminHistoryRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminHistory::class);
    }

    public function getLastItems(int $limit, array $parameters): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('ah')
            ->from(AdminHistory::class, 'ah')
            ->orderBy('ah.time', 'DESC')
            ->setMaxResults($limit) // Set the maximum number of results
            ->getQuery()
            ->getResult();
    }
}
