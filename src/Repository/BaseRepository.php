<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM as ORM;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\DefaultView;

abstract class BaseRepository extends ServiceEntityRepository
{
    public function getRandom(int $limit){
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('p')
            ->from($this->getClassName(), 'p')
            ->addSelect('RAND() as HIDDEN rand')
            ->orderBy('rand')
            ->setMaxResults( $limit );

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getPaginated(array $items, int $items_per_page, int $page, string $basePath, $order, $params = []) {
        $adapter = new ArrayAdapter($items);
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta->setMaxPerPage($items_per_page); // 10 by default
        $maxPerPage = $pagerfanta->getMaxPerPage();

        $pagerfanta->setCurrentPage($page); // 1 by default
        $currentPage = $pagerfanta->getCurrentPage();

        $nbResults = $pagerfanta->getNbResults();
        $currentPageResults = $pagerfanta->getCurrentPageResults();

        $view = new DefaultView();
        $options = array(
            'proximity' => 5,
            'prev_message' => '< Previous',
            'next_message' => 'Next >',
            );
        $routeGenerator = function($page) use ($basePath, $order, $items_per_page, $params) {
            $path = "$basePath?page=$page";
            if(!empty($order)) {
                $path .= "&orderBy=".array_key_first($order)."&orientation=".$order[array_key_first($order)]."&nbItems=".$items_per_page;
            }
            if(!empty($params)) {
                foreach ($params as $paramName => $paramValue) {
                    $path .= "&$paramName=$paramValue";
                }
            }
            return $path;
        };
        $html = $view->render($pagerfanta, $routeGenerator, $options);

        return ['results' => $currentPageResults, 'count' => $nbResults, 'pager' => $html];
    }

    public function findByJoinField(array $subParam, array $order) {
        return $this->createQueryBuilder('a')
            ->leftJoin('a.'.$subParam[0], 'b')
            ->orderBy('CASE WHEN b.'.$subParam[1].' IS NULL THEN a.id ELSE b.'.$subParam[1].' END', $order[array_key_first($order)])
            ->getQuery()
            ->getResult();
    }

    public function countItems(): int
    {
        try {
            $queryBuilder = $this->getEntityManager()->createQueryBuilder()
                ->select('COUNT(e.id)')
                ->from($this->getClassName(), 'e');

            $result = $queryBuilder->getQuery()->getSingleScalarResult();

            return (int) $result;
        } catch (ORM\Query\QueryException $e) {
            throw new \Exception('Error executing the count query: ' . $e->getMessage());
        } catch (ORM\NonUniqueResultException $e) {
            throw new \Exception('Error retrieving the count result: ' . $e->getMessage());
        } catch (ORM\NoResultException $e) {
            return 0;
        } catch (\Exception $e) {
            throw new \Exception('Error executing the count query: ' . $e->getMessage());
        }
    }
}
