<?php

namespace App\Repository;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findUnreadNotifications(User $user): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('n')
            ->from(Notification::class, 'n')
            ->where('n.recipient = :user')
            ->andWhere('n.status = :unread OR n.status = :new')
            ->setParameter('user', $user)
            ->setParameter('unread', 'unread')
            ->setParameter('new', 'new')
            ->orderBy('n.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function sortUsersByRole(array $users, $orientation): array
    {
        usort($users, function ($user1, $user2) {
            $user1HasAdminRole = in_array('ROLE_ADMIN', $user1->getRoles());
            $user2HasAdminRole = in_array('ROLE_ADMIN', $user2->getRoles());

            if ($user1HasAdminRole && !$user2HasAdminRole) {
                return -1; // $user1 comes before $user2
            } elseif (!$user1HasAdminRole && $user2HasAdminRole) {
                return 1; // $user2 comes before $user1
            } else {
                return 0; // no change in order
            }
        });

        if($orientation === 'desc') {
            $users = array_reverse($users);
        }

        return $users;
    }

    public function findUsersByRole(string $role): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.roles LIKE :role')
            ->setParameter('role', '%'.$role.'%')
            ->getQuery()
            ->getResult();
    }

    public function getLastItems(int $limit, array $parameters): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.is_active = :is_active')
            ->setParameter('is_active', $parameters['active_only'])
            ->orderBy('u.created_at', 'DESC')
            ->setMaxResults($limit) // Set the maximum number of results
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
