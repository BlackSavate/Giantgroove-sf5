<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function findLastItemOfType(string $type) {
        return $this->createQueryBuilder('n')
            ->andWhere('n.type = :val')
            ->setParameter('val', $type)
            ->orderBy('n.counter', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
    public function getLastItems(int $limit, array $parameters): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('n')
            ->from(News::class, 'n')
            ->orderBy('n.created_at', 'DESC')
            ->setMaxResults($limit) // Set the maximum number of results
            ->getQuery()
            ->getResult();
    }
}
