<?php

namespace App\Service;

use Pusher\Pusher;

class PusherService {
    private $pusher;

    public function __construct(string $appKey, string $appSecret, string $appId, string $appCluster) {
        $options = [
            'cluster' => $appCluster,
            'useTLS' => true
        ];
        $this->pusher = new Pusher(
            $appKey,
            $appSecret,
            $appId,
            $options
        );
    }

    public function trigger(string $channel, string $event, array $data) {
        $this->pusher->trigger($channel, $event, $data);
    }
}
