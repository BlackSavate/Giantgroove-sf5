<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, string $folder): string {
        $fileName =$this->generateUniqueFileName().'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory($folder), $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }

    public function getTargetDirectory(string $folder = ""): string {
        if (!empty($folder)) {
            switch ($folder) {
                case 'avatar':
                    $folder = 'avatar';
                    break;
                case 'band_logo':
                    $folder = 'band/logo';
                    break;
                case 'band_banner':
                    $folder = 'band/banner';
                    break;
                default:
            }
        }
        return $this->targetDirectory."/$folder";
    }

    /**
     * @return string
     */
    protected function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
