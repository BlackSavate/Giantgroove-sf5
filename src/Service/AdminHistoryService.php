<?php

namespace App\Service;

use App\Entity\AdminHistory;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class AdminHistoryService {

    private const ACTIONS = [
        'enable', 'disable',
        'add_admin', 'remove_admin',
        'create','update', 'delete',
    ];

    private const TARGET_TYPE = [
        'user',
        'band',
        'project',
        'style',
        'instrument',
        'news',
    ];

    private $entityManager;
    private $router;

    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router) {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function addHistory(User $user, string $action, string $targetType, int $targetId, string $targetLabel) {
        if (!$user instanceof User) {
            return new Response('Not a User', 404);
        }
        if(!in_array($action, self::ACTIONS)) {
            return new Response('Not a valid action', 404);
        }
        if(!in_array($targetType, self::TARGET_TYPE)) {
            return new Response('Not a target type', 404);
        }

        $em = $this->entityManager;
        $targetClass = ucfirst($targetType);
        $targetRepository = $em->getRepository("App\\Entity\\$targetClass");
        $target = $targetRepository->find($targetId);
        if(!$target && $action !== 'delete') {
            return new Response("$targetType $targetId does'nt exists" , 404);
        }

        $currentDate = new \Datetime();
        $historyItem = new AdminHistory();
        $historyItem->setUserId($user->getId());
        $historyItem->setUsername($user->getUsername());
        $historyItem->setAction($action);
        $historyItem->setTargetType($targetType);
        $historyItem->setTargetId($targetId);
        $historyItem->setTargetLabel($targetLabel);
        $historyItem->setTime($currentDate);
        $em->persist($historyItem);
        $em->flush();

        return new Response(null, Response::HTTP_CREATED);
    }

    public function getMessage(AdminHistory $id) {

        $em = $this->entityManager;

        $historyRepository = $em->getRepository(AdminHistory::class);
        $userRepository = $em->getRepository(User::class);
        $historyItem = $historyRepository->find($id);

        $userId = $historyItem->getUserId();
        $username = $historyItem->getUsername();

        $user = $userRepository->find($userId);
        if ($user instanceof User) {
            $userLink = $this->router->generate('user_detail', ['slug' => $user->getSlug()]);
        }

        $time = $historyItem->getTime()->format('Y-m-d H:i:s');
        $action = $historyItem->getAction();
        $type = $historyItem->getTargetType();
        $targetId = $historyItem->getTargetId();
        $targetLabel = $historyItem->getTargetLabel();

        $targetClass = ucfirst($type);
        $targetRepository = $em->getRepository("App\\Entity\\$targetClass");
        $target = $targetRepository->find($targetId);

        $route= $type . '_detail';
        if(in_array($type, ['instrument', 'style', 'news'])) {
            $route = 'admin_'.$route;
        }


        if($target) {
            $targetLink = $this->router->generate($route, ['slug' => $target->getSlug()]);;
        }

        $actionColor = [
            'enable' => 'green',
            'disable' => 'red',
            'add_admin' => 'gold',
            'remove_admin' => 'red',
            'create' => 'green',
            'update' => 'blue',
            'delete' => 'red',
        ];

        $actionText = [
            'enable' => "has <span class='$actionColor[$action]'>enabled</span>",
            'disable' => "has <span class='$actionColor[$action]'>disabled</span>",
            'add_admin' => "<span class='$actionColor[$action]'>added</span> admin privileges to",
            'remove_admin' => "<span class='$actionColor[$action]'>removed</span> admin privileges to",
            'create' => "has <span class='$actionColor[$action]'>created</span> a new",
            'update' => "has <span class='$actionColor[$action]'>updated</span> the",
            'delete' => "has <span class='$actionColor[$action]'>deleted</span> the",
        ];

        $targetTypeColor = 'white';

        $msg = "";
        $msg .= " <span class='history-time'>$time</span>";
        $msg .= " <span class='history-detail'>";
        $msg .= " <span class='history-user'>$username";
        $msg .= " (<span class='history-user-id'>";
        $msg .= (isset($userLink)) ? "<a href='$userLink'>$userId</a>" : $userId;
        $msg .= "</span>";
        $msg .= ")</span> ";
        $msg .= " <span class='history-action'>$actionText[$action]</span> ";
        $msg .= " <span class='history-target-type $targetTypeColor'>".ucfirst($type)."</span> ";
        $msg .= " <span class='history-target-label'>\"$targetLabel\"</span> ";
        $msg .= " (<span class='history-target-id'>";
        $msg .= (isset($targetLink)) ? "<a href='$targetLink'>$targetId</a>" : $targetId;
        $msg .= "</span>";
        $msg .= ")</span>";

        return $msg;
    }
}
