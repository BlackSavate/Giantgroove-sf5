var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')
    .copyFiles({
      from: 'node_modules/tinymce/skins',
      to: './skins/[path][name].[ext]'
    })
    .copyFiles({
      from: 'assets/css/tinymce/custom',
      to: './skins/custom/[path][name].[ext]'
    })
    .copyFiles({
      from: 'node_modules/tinymce/icons',
      to: './icons/[path][name].[ext]'
    })
    .copyFiles({
      from: 'node_modules/tinymce/models',
      to: './models/[path][name].[ext]'
    })

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('reset', './assets/css/reset.css')
    .addEntry('normalize', './assets/css/normalize.css')
    .addEntry('menu', './assets/css/menu.scss')
    .addEntry('home', './assets/css/home.scss')
    .addEntry('app', ['./assets/js/app.js','./assets/css/app.scss'])
    .addEntry('profile', './assets/css/profile.scss')
    .addEntry('admin', './assets/js/admin.js')
    .addEntry('notifications', './assets/js/notification.js')
    .addEntry('band', './assets/js/band.js')
    .addEntry('project', './assets/js/project.js')
    .addEntry('mixing', './assets/js/mixing.js')
    .addEntry('tinymce', './node_modules/tinymce/tinymce.js')
    .addEntry('bootstrap', './node_modules/bootstrap/js/dist/modal.js')
    .autoProvidejQuery()
    .enableSingleRuntimeChunk()
    .addStyleEntry('tinymce-style', './node_modules/tinymce/skins/ui/oxide/skin.css')
    .addStyleEntry('tinymce-content-style', './node_modules/tinymce/skins/content/default/content.css')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })

    // enables Sass/SCSS support
    .enableSassLoader()
;

module.exports = Encore.getWebpackConfig();
