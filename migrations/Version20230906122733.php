<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230906122733 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin_history DROP FOREIGN KEY FK_E386F2C2A76ED395');
        $this->addSql('DROP INDEX IDX_E386F2C2A76ED395 ON admin_history');
        $this->addSql('ALTER TABLE admin_history CHANGE user_id user_id INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin_history CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE admin_history ADD CONSTRAINT FK_E386F2C2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_E386F2C2A76ED395 ON admin_history (user_id)');
    }
}
